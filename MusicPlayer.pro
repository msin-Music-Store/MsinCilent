QT       += core gui network multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Controller/artistcontrol.cpp \
    Controller/musicinfocontrol.cpp \
    Controller/musicplaycontrol.cpp \
    Controller/usercontrol.cpp \
    DataAccess/Artist/artistinfoimp.cpp \
    DataAccess/Artist/artistnetworkimp.cpp \
    DataAccess/Music/musicinfoimp.cpp \
    DataAccess/Music/musicnetworkimp.cpp \
    DataAccess/User/mysonglistnetwork.cpp \
    DataAccess/User/userinfoimp.cpp \
    DataAccess/User/usernetworkimp.cpp \
    DataAccess/imagedownload.cpp \
    DataAccess/jsoninfointerface.cpp \
    DataAccess/networkinterface.cpp \
    Entity/artist.cpp \
    Entity/music.cpp \
    Entity/user.cpp \
    Test/testwindow.cpp \
    UI/logindialog.cpp \
    UI/mainwindow.cpp \
    UI/registerdialog.cpp \
    main.cpp

HEADERS += \
    Controller/artistcontrol.h \
    Controller/musicinfocontrol.h \
    Controller/musicplaycontrol.h \
    Controller/usercontrol.h \
    DataAccess/Artist/artistinfoimp.h \
    DataAccess/Artist/artistnetworkimp.h \
    DataAccess/Music/musicinfoimp.h \
    DataAccess/Music/musicnetworkimp.h \
    DataAccess/User/mysonglistnetwork.h \
    DataAccess/User/userinfoimp.h \
    DataAccess/User/usernetworkimp.h \
    DataAccess/imagedownload.h \
    DataAccess/jsoninfointerface.h \
    DataAccess/networkinterface.h \
    Entity/artist.h \
    Entity/music.h \
    Entity/user.h \
    Test/testwindow.h \
    UI/logindialog.h \
    UI/mainwindow.h \
    UI/registerdialog.h

FORMS += \
    Test/testwindow.ui \
    UI/logindialog.ui \
    UI/mainwindow.ui \
    UI/registerdialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc
