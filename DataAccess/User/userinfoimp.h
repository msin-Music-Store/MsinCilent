#ifndef USERINFOIMP_H
#define USERINFOIMP_H

#include <QObject>
#include "DataAccess/jsoninfointerface.h"
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonParseError>
#include "Entity/user.h"
class UserInfoImp : public QObject,public JsonInfoInterface
{
    Q_OBJECT
public:
    explicit UserInfoImp(QObject *parent = nullptr);
    void getInfo();
    User getUser();
    bool getRegisterInfo();
    QStringList getLoginInfo();
signals:

private:
    User user;
};

#endif // USERINFOIMP_H
