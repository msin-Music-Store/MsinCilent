#ifndef USERNETWORKIMP_H
#define USERNETWORKIMP_H

#include <QObject>
#include "DataAccess/networkinterface.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonObject>
#include <QJsonDocument>
#include <qDebug>
#include "Entity/user.h"
class UserNetworkImp : public QObject,public NetworkInterface
{
    Q_OBJECT
public:
    explicit UserNetworkImp(QObject *parent = nullptr);
    void setArray();
    void setLoginArray(QString userName,QString password);
    void setUserInfoArray(QString token);
    void setToken(QString token);
    void setMode(int index);
    void registerUser(User user);
    void updateUser(User user,QString token);
    void createMysongList(int id,QString name,QString token);
signals:
    void downLoadFinish(QByteArray array);
private:
    QString token;
};

#endif // USERNETWORKIMP_H
