#include "mysonglistnetwork.h"

MysongListNetwork::MysongListNetwork(QObject *parent) : QObject(parent)
{

}
void MysongListNetwork::setArray()
{

}
void MysongListNetwork::createSongList(int id,QString name,QString token)
{
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    qDebug()<<url;
    qDebug()<<token;
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json"));
    request.setRawHeader("Authorization", token.toUtf8());
    QJsonObject obj;
    obj.insert("id",id);
    obj.insert("name",name);
    obj.insert("imageUrl","/default.jpg");
    QByteArray postData=QJsonDocument(obj).toJson();
    reply=manager->post(request,postData);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
    });
}
