#include "userinfoimp.h"

UserInfoImp::UserInfoImp(QObject *parent) : QObject(parent)
{

}

QStringList UserInfoImp::getLoginInfo()
{
    QStringList result;
    int code=0;
    QString token="";
    QString errorMessage="";
    //解析JSON数据
    QJsonParseError jsonError;
    QJsonDocument doucment = QJsonDocument::fromJson(array, &jsonError);  // 转化为 JSON 文档
    if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError)) {  // 解析未发生错误
        if (doucment.isObject()) { // JSON 文档为对象
            QJsonObject object = doucment.object();  // 转化为对象
            if (object.contains("code")) {  // 包含指定的 key
                QJsonValue value = object.value("code");  // 获取指定 key 对应的 value
                if (value.isDouble()) {  // 判断 value 是否为字符串
                    code = value.toInt();  // 将 value 转化为字符串
                }
            }
            if (object.contains("data")) {
                QJsonValue value = object.value("data");
                if (value.isString()) {
                    token = value.toVariant().toString();
                }
            }
            if (object.contains("errorMessage")) {
                QJsonValue value = object.value("errorMessage");
                if (value.isString()) {
                    errorMessage = value.toString();
                }
            }
        }
    }

    if(code==200)
    {
        result.append(QString("%1").arg(code));
        result.append(token);
    }else{
        result.append(QString("%1").arg(code));
        result.append(errorMessage);
    }

    return result;
}

User UserInfoImp::getUser()
{
    return user;
}

void UserInfoImp::getInfo()
{
    QMap<QString,QString> map;
    int code=0;
    int userId=0;
    QString errorMessage="";
    //解析JSON数据
    QJsonParseError jsonError;
    QJsonDocument doucment = QJsonDocument::fromJson(array, &jsonError);  // 转化为 JSON 文档
    if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError)) {  // 解析未发生错误
        if (doucment.isObject()) { // JSON 文档为对象
            map.clear();
            QJsonObject object = doucment.object();  // 转化为对象
            if (object.contains("code")) {  // 包含指定的 key
                QJsonValue value = object.value("code");  // 获取指定 key 对应的 value
                if (value.isDouble()) {  // 判断 value 是否为double
                    code = value.toInt();  // 将 value 转化为int
                }
            }

            if (object.contains("data")) {
                QJsonValue obj = object.value("data");
                if (obj.isObject()) {
                    QJsonObject info=obj.toObject();
                    if(info.contains("id"))
                    {
                        QJsonValue id=info.value("id");
                        if(id.isDouble())
                        {
                            userId=id.toInt();
                        }
                    }
                    if(info.contains("username"))
                    {
                        QJsonValue username=info.value("username");
                        if(username.isString())
                        {
                            map.insert("username",username.toVariant().toString());
                        }
                    }
                    if(info.contains("email"))
                    {
                        QJsonValue email=info.value("email");
                        if(email.isString())
                        {
                            if(email.toVariant().toString().isEmpty())
                            {
                                map.insert("email",NULL);
                            }else{
                                 map.insert("email",email.toVariant().toString());
                            }
                        }
                    }
                    if(info.contains("headPhoto"))
                    {
                        QJsonValue headPhoto=info.value("headPhoto");
                        if(headPhoto.isString())
                        {
                            map.insert("headPhoto",headPhoto.toVariant().toString());
                        }
                    }
                    if(info.contains("location"))
                    {
                        QJsonValue location=info.value("location");
                        if(location.toVariant().toString().isEmpty())
                        {
                            map.insert("location",NULL);
                        }else{
                            map.insert("location",location.toVariant().toString());
                        }
                    }
                }
            }
            if (object.contains("errorMessage")) {
                QJsonValue value = object.value("errorMessage");
                if (value.isString()) {
                    errorMessage = value.toString();
                    qDebug() << "errorMessage : " << errorMessage;
                }
            }
        }
    }

    if(code==200)
    {
        user.setId(userId);
        user.setUserName(map.value("username"));
        user.setEmail(map.value("email"));
        user.setHeadUrl(map.value("headPhoto"));
        user.setLocation(map.value("location"));
    }
}

bool UserInfoImp::getRegisterInfo()
{
    QStringList result;
    int code=0;
    QString token="";
    QString errorMessage="";
    //解析JSON数据
    QJsonParseError jsonError;
    QJsonDocument doucment = QJsonDocument::fromJson(array, &jsonError);  // 转化为 JSON 文档
    if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError)) {  // 解析未发生错误
        if (doucment.isObject()) { // JSON 文档为对象
            QJsonObject object = doucment.object();  // 转化为对象
            if (object.contains("code")) {  // 包含指定的 key
                QJsonValue value = object.value("code");  // 获取指定 key 对应的 value
                if (value.isDouble()) {  // 判断 value 是否为字符串
                    code = value.toInt();  // 将 value 转化为字符串
                }
            }
            if (object.contains("data")) {
                QJsonValue value = object.value("data");
                if (value.isString()) {
                    token = value.toVariant().toString();
                }
            }
            if (object.contains("errorMessage")) {
                QJsonValue value = object.value("errorMessage");
                if (value.isString()) {
                    errorMessage = value.toString();
                }
            }
        }
    }

    if(code==200)
    {
        return true;
    }else{
        return false;
    }
}
