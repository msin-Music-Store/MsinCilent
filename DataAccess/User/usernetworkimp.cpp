#include "usernetworkimp.h"

UserNetworkImp::UserNetworkImp(QObject *parent) : QObject(parent)
{
    this->manager=new QNetworkAccessManager(this);
    this->reply=NULL;
}

void UserNetworkImp::setArray()
{

}

void UserNetworkImp::setLoginArray(QString userName,QString password)
{
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json"));
    QJsonObject obj;
    obj.insert("username",userName);
    obj.insert("password",password);
    QByteArray postData=QJsonDocument(obj).toJson();
    reply=manager->post(request,postData);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
        array=reply->readAll();
        emit downLoadFinish(array);
    });
}
void UserNetworkImp::setToken(QString token)
{
    this->token=token;
}


void UserNetworkImp::setUserInfoArray(QString token)
{
    QNetworkRequest request;
    request.setUrl(QUrl("http://127.0.0.1:5000/User/info"));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json")); //设置数据类型
    request.setRawHeader("Authorization", token.toUtf8());
    reply=manager->get(request);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
        array=reply->readAll();
        emit downLoadFinish(array);
    });
}

void UserNetworkImp::registerUser(User user)
{
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json"));
    QJsonObject obj;
    obj.insert("username",user.getUserName());
    obj.insert("password",user.getPassWord());
    obj.insert("email",user.getEmail());
    obj.insert("confirePassword",user.getPassWord());
    obj.insert("headPhoto",user.getHeadUrl());
    obj.insert("location",user.getLocation());
    QByteArray postData=QJsonDocument(obj).toJson();
    reply=manager->post(request,postData);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
        array=reply->readAll();
        emit downLoadFinish(array);
    });
}

void UserNetworkImp::updateUser(User user,QString token)
{
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    qDebug()<<url;
    qDebug()<<user.getPassWord();
    qDebug()<<user.getEmail();
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json"));
    request.setRawHeader("Authorization", token.toUtf8());
    QJsonObject obj;
    obj.insert("id",user.getId());
    obj.insert("username",user.getUserName());
    obj.insert("password",user.getPassWord());
    obj.insert("email",user.getEmail());
    obj.insert("userType","user");
    obj.insert("headPhoto",user.getHeadUrl());
    obj.insert("location",user.getLocation());
    obj.insert("removed",true);
    QByteArray postData=QJsonDocument(obj).toJson();
    reply=manager->post(request,postData);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
        array=reply->readAll();
        emit downLoadFinish(array);
        qDebug()<<QString(array);
    });
}

void UserNetworkImp::createMysongList(int id,QString name,QString token)
{

}
