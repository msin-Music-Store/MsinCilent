#ifndef MYSONGLISTNETWORK_H
#define MYSONGLISTNETWORK_H

#include <QObject>
#include "DataAccess/networkinterface.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonObject>
#include <QJsonDocument>
class MysongListNetwork : public QObject,public NetworkInterface
{
    Q_OBJECT
public:
    explicit MysongListNetwork(QObject *parent = nullptr);
    void setArray();
    void createSongList(int id,QString name,QString token);
signals:

};

#endif // MYSONGLISTNETWORK_H
