#include "imagedownload.h"

ImageDownload::ImageDownload(QObject *parent) : QObject(parent)
{
    manager=new QNetworkAccessManager(this);
    reply=NULL;
}

void ImageDownload::downImageFile()
{
    if(index==imageList.size())
    {
        index=0;
        emit downLoadImageFinish();
        return;
    }else{
        QNetworkRequest request;
        request.setUrl(QUrl(imageList.at(index)));
        request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json"));
        reply=manager->get(request);
        connect(reply,&QNetworkReply::readyRead,this,[=](){
            QByteArray array=reply->readAll();
            QString filenamePath=QString("C://Users//Public//MusicPlayer//Image//%1").arg(imageList.at(index).mid(28));
            //实例QFile
            QFile file(filenamePath);

            //存在打开，不存在创建
            file.open(QIODevice::ReadWrite | QIODevice::Append);
            //写入QByteArray格式字符串
            file.write(array, array.size());
            file.flush();

            //关闭文件
            file.close();
            index++;
            downImageFile();
        });
    }
}

void ImageDownload::postImageFile(QString path)
{
    QNetworkRequest request;
    //request初始化
    //发送multi part内容时，不能设置下面一句，否则服务端无法解析边界
    //request.setHeader(QNetworkRequest::ContentTypeHeader, "multipart/form-data");
    request.setUrl(QUrl("http://127.0.0.1:5000/Image"));
    QFile* file = new QFile(path);
    file->open(QIODevice::ReadOnly);
    QHttpMultiPart *multi_part = new QHttpMultiPart(QHttpMultiPart::FormDataType);
    QHttpPart image_part;
    image_part.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpg"));
    image_part.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(QString("form-data; name=\"files\";filename=\"testfile.jpg\";")));
    image_part.setBodyDevice(file);
    multi_part->append(image_part);

    reply = manager->post(request, multi_part);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
       QByteArray array=reply->readAll();
       emit postImageFileFinsh(array);
    });
}

void ImageDownload::setImageUrlList(QStringList list)
{
    this->imageList=list;
}
