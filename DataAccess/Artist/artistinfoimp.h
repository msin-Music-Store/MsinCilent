#ifndef ARTISTINFOIMP_H
#define ARTISTINFOIMP_H

#include <QObject>
#include "DataAccess/jsoninfointerface.h"
#include "Entity/artist.h"
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonParseError>

class ArtistInfoImp : public QObject,public JsonInfoInterface
{
    Q_OBJECT
public:
    explicit ArtistInfoImp(QObject *parent = nullptr);
    void getInfo();
    void getInfoById();
    QList<Artist> getArtistListByclas();
    Artist getArtist();
signals:
private:
    QList<Artist> artistList;
    Artist artist;
};

#endif // ARTISTINFOIMP_H
