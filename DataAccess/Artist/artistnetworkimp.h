#ifndef ARTISTNETWORKIMP_H
#define ARTISTNETWORKIMP_H

#include <QObject>
#include "DataAccess/networkinterface.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QDebug>

class ArtistNetworkImp : public QObject,public NetworkInterface
{
    Q_OBJECT
public:
    explicit ArtistNetworkImp(QObject *parent = nullptr);
    void setArray();
signals:
    void downInfoFinished(QByteArray array);
};

#endif // ARTISTNETWORKIMP_H
