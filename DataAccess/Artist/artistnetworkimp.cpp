#include "artistnetworkimp.h"

ArtistNetworkImp::ArtistNetworkImp(QObject *parent) : QObject(parent)
{
    this->manager=new QNetworkAccessManager(this);
    this->reply=NULL;
}
void ArtistNetworkImp::setArray()
{
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json"));
    reply=manager->get(request);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
        array=reply->readAll();
        emit downInfoFinished(array);
    });
}
