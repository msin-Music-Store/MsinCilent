#include "artistinfoimp.h"

ArtistInfoImp::ArtistInfoImp(QObject *parent) : QObject(parent)
{

}
void ArtistInfoImp::getInfo()
{
    artistList.clear();
    //解析JSON数据
    QJsonParseError jsonError;
    QJsonDocument doucment = QJsonDocument::fromJson(array, &jsonError);  // 转化为 JSON 文档
    if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError)) {  // 解析未发生错误

        if (doucment.isArray()) { // JSON 文档为数组
            QJsonArray objectArray = doucment.array();  // 转化为对象

            for(int i=0;i<objectArray.size();i++)
            {
                Artist artist;
                QJsonValue value=objectArray.at(i);
                QJsonObject object=value.toObject();
                if(object.contains("id"))
                {
                    QJsonValue id=object.value("id");
                    if(id.isDouble())
                    {
                        artist.setId(id.toInt());
                    }
                }
                if(object.contains("name"))
                {
                    QJsonValue name=object.value("name");
                    if(name.isString())
                    {
                        artist.setName(name.toVariant().toString());
                    }
                }
                if(object.contains("imageUrl"))
                {
                    QJsonValue imageUrl=object.value("imageUrl");
                    if(imageUrl.isString())
                    {
                        artist.setImage_url(imageUrl.toVariant().toString().toUtf8());
                    }
                }
                artistList.append(artist);
            }
        }
    }
}

void ArtistInfoImp::getInfoById()
{
    //解析JSON数据
    Artist newartist;
    QJsonParseError jsonError;
    QJsonDocument doucment = QJsonDocument::fromJson(array, &jsonError);  // 转化为 JSON 文档
    if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError)) {  // 解析未发生错误
        if(doucment.isObject())
        {
            QJsonObject object=doucment.object();
            if(object.contains("id"))
            {
                QJsonValue id=object.value("id");
                if(id.isDouble())
                {
                    newartist.setId(id.toInt());
                }
            }
            if(object.contains("name"))
            {
                QJsonValue name=object.value("name");
                if(name.isString())
                {
                    newartist.setName(name.toVariant().toString());
                }
            }
            if(object.contains("sex"))
            {
                QJsonValue sex=object.value("sex");
                if(sex.isString())
                {
                    newartist.setSex(sex.toVariant().toString().toUtf8());
                }
            }
            if(object.contains("imageUrl"))
            {
                QJsonValue imageUrl=object.value("imageUrl");
                if(imageUrl.isString())
                {
                    newartist.setImage_url(imageUrl.toVariant().toString().toUtf8());
                }
            }
            if(object.contains("nation"))
            {
                QJsonValue nation=object.value("nation");
                if(nation.isString())
                {
                    newartist.setNation(nation.toVariant().toString().toUtf8());
                }
            }
            if(object.contains("birthday"))
            {
                QJsonValue birthday=object.value("birthday");
                if(birthday.isString())
                {
                    newartist.setBirthday(birthday.toVariant().toString().toUtf8());
                }
            }
        }
    }
    artist=newartist;
}

Artist ArtistInfoImp::getArtist()
{
    return artist;
}

QList<Artist> ArtistInfoImp::getArtistListByclas()
{
    return artistList;
}
