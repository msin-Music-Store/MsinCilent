#include "musicinfoimp.h"

MusicInfoImp::MusicInfoImp(QObject *parent) : QObject(parent)
{

}
void MusicInfoImp::getInfo()
{
    musicList.clear();
   //解析JSON数据
   QJsonParseError jsonError;
   QJsonDocument doucment = QJsonDocument::fromJson(array, &jsonError);  // 转化为 JSON 文档
   if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError)) {  // 解析未发生错误

       if (doucment.isArray()) { // JSON 文档为数组
           QJsonArray objectArray = doucment.array();  // 转化为对象

           for(int i=0;i<objectArray.size();i++)
           {
               Music music;
               QJsonValue value=objectArray.at(i);
               QJsonObject object=value.toObject();
               if(object.contains("id"))
               {
                   QJsonValue id=object.value("id");
                   if(id.isDouble())
                   {
                       music.setId(id.toInt());
                   }
               }
               if(object.contains("name"))
               {
                   QJsonValue name=object.value("name");
                   if(name.isString())
                   {
                       music.setMusicName(name.toVariant().toString());
                   }
               }
               if(object.contains("musicUrl"))
               {
                   QJsonValue musicUrl=object.value("musicUrl");
                   if(musicUrl.isString())
                   {
                       music.setFileUrl(musicUrl.toVariant().toString().toUtf8());
                   }
               }

               if(object.contains("artistName"))
               {
                   QJsonValue artistName=object.value("artistName");
                   if(artistName.isString())
                   {
                       music.setArtistName(artistName.toVariant().toString().toUtf8());
                   }
               }
               if(object.contains("albumName"))
               {
                   QJsonValue albumName=object.value("albumName");
                   if(albumName.isString())
                   {
                       music.setAlbumName(albumName.toVariant().toString().toUtf8());
                   }
               }
               if(object.contains("songLength"))
               {
                   QJsonValue songLength=object.value("songLength");
                   if(songLength.isString())
                   {
                      music.setMusicSize(songLength.toVariant().toString().toUtf8());
                   }
               }
               if(object.contains("classificationName"))
               {
                   QJsonValue classificationName=object.value("classificationName");
                   if(classificationName.isString())
                   {
                       music.setClassificationName(classificationName.toVariant().toString().toUtf8());
                   }
               }

               musicList.append(music);
           }
       }
   }
}

int MusicInfoImp::getUserSongList()
{
    QStringList list;
   //解析JSON数据
   QJsonParseError jsonError;
   QJsonDocument doucment = QJsonDocument::fromJson(array, &jsonError);  // 转化为 JSON 文档
   if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError)) {  // 解析未发生错误

       if (doucment.isArray()) { // JSON 文档为数组
           QJsonArray objectArray = doucment.array();  // 转化为对象

           for(int i=0;i<objectArray.size();i++)
           {
               Music music;
               QJsonValue value=objectArray.at(i);
               QJsonObject object=value.toObject();
               if(object.contains("id"))
               {
                   QJsonValue id=object.value("id");
                   if(id.isDouble())
                   {
                       list.append(QString::number(id.toInt()));
                   }
               }
               if(object.contains("name"))
               {
                   QJsonValue name=object.value("name");
                   if(name.isString())
                   {
                       list.append(name.toVariant().toString());
                   }
               }
               if(object.contains("imageUrl"))
               {
                   QJsonValue musicUrl=object.value("imageUrl");
                   if(musicUrl.isString())
                   {
                       list.append(musicUrl.toVariant().toString().toUtf8());
                   }
               }
           }
       }
   }
   return list.at(0).toInt();
}

QList<Music> MusicInfoImp::getMusicList()
{
    return musicList;
}
