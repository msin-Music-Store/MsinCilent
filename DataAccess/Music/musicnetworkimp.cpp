#include "musicnetworkimp.h"

MusicNetworkImp::MusicNetworkImp(QObject *parent) : QObject(parent)
{
    this->manager=new QNetworkAccessManager(this);
    this->reply=NULL;
}
void MusicNetworkImp::setArray()
{
    QNetworkRequest request;
    this->array.clear();
    request.setUrl(QUrl(url));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json")); //设置数据类型
    reply=manager->get(request);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
        array=reply->readAll();
        emit downInfoFinished(array);
    });
}
void MusicNetworkImp::setRecommendArray(QString token) //获取歌曲推荐列表
{
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json")); //设置数据类型
    request.setRawHeader("Authorization", token.toUtf8());
    reply=manager->get(request);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
        array=reply->readAll();
        emit downInfoFinished(array);
    });
}
void MusicNetworkImp::setSongListArray(QString token)  //获取用户歌单
{
    QNetworkRequest request;
    request.setUrl(QUrl("http://127.0.0.1:5000/SongList"));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json")); //设置数据类型
    request.setRawHeader("Authorization", token.toUtf8());
    reply=manager->get(request);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
        array=reply->readAll();
        emit downInfoFinished(array);
        qDebug()<<array;
    });
}
void MusicNetworkImp::setCollectArray(QString token,int id)  //获取用户歌单下歌曲列表
{
    QNetworkRequest request;
    QString newurl=QString("http://127.0.0.1:5000/SongList/%1/musics").arg(id);
    request.setUrl(QUrl(newurl));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json")); //设置数据类型
    request.setRawHeader("Authorization", token.toUtf8());
    reply=manager->get(request);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
        array=reply->readAll();
        emit downInfoFinished(array);
    });
}

void MusicNetworkImp::setSearchArray()
{
    QNetworkRequest request;
    this->array.clear();
    request.setUrl(QUrl(url));
    reply=manager->get(request);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
        array.append(reply->readAll());
        emit downInfoFinished(array);
    });
}

void MusicNetworkImp::insertMusicToSongList(QString token,int music_id,int songList_id)
{
    QNetworkRequest request;
    QJsonObject obj;
    obj.insert("musicId",music_id);
    obj.insert("songListId",songList_id);
    QByteArray postData=QJsonDocument(obj).toJson();
    request.setUrl(QUrl("http://127.0.0.1:5000/SongList/CreateSongToSongList"));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json")); //设置数据类型
    request.setRawHeader("Authorization", token.toUtf8());
    reply=manager->post(request,postData);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
        qDebug()<<reply->readAll();
    });
}

void MusicNetworkImp::deleteMusicFromSongList(QString token,int music_id,int songList_id)
{
    QNetworkRequest request;
    QJsonObject obj;
    obj.insert("musicId",music_id);
    obj.insert("songListId",songList_id);
    QByteArray postData=QJsonDocument(obj).toJson();
    request.setUrl(QUrl("http://127.0.0.1:5000/SongList/DeleteSongFromSongList"));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json")); //设置数据类型
    request.setRawHeader("Authorization", token.toUtf8());
    reply=manager->post(request,postData);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
        qDebug()<<reply->readAll();
    });
}

void MusicNetworkImp::insertListenRecord(QString token)
{
    QNetworkRequest request;
    QJsonObject obj;
    request.setUrl(QUrl(url));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json")); //设置数据类型
    request.setRawHeader("Authorization", token.toUtf8());
    reply=manager->get(request);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
        qDebug()<<reply->readAll();
    });
}

void MusicNetworkImp::downLoadMusic(QString newurl,QString musicName)
{
    QNetworkRequest request;
    request.setUrl(QUrl(newurl));
    QDateTime timeCurrent = QDateTime::currentDateTime();
    QString time = timeCurrent.toString("yyyy-MM-dd hh-mm-ss");
    QString fileNAME=QString("%1    %2.mp3").arg(musicName).arg(time);
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json")); //设置数据类型
    reply=manager->get(request);
    connect(reply,&QNetworkReply::readyRead,this,[=](){
        array=reply->readAll();
        QString filenamePath=QString("C://Users//Public//MusicPlayer//Music//%1.mp3").arg(fileNAME);
        //实例QFile
        QFile file(filenamePath);

        //存在打开，不存在创建
        file.open(QIODevice::ReadWrite | QIODevice::Append);
        //写入QByteArray格式字符串
        file.write(array, array.size());
        file.flush();

        //关闭文件
        file.close();
    });
}

