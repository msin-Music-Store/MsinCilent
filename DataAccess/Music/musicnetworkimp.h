#ifndef MUSICNETWORKIMP_H
#define MUSICNETWORKIMP_H

#include <QObject>
#include "DataAccess/networkinterface.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonObject>
#include <QJsonDocument>
#include <QDebug>
#include <QFile>

class MusicNetworkImp : public QObject,public NetworkInterface
{
    Q_OBJECT
public:
    explicit MusicNetworkImp(QObject *parent = nullptr);
    void setArray();
    void setRecommendArray(QString token);
    void setSongListArray(QString token);
    void setSearchArray();
    void setCollectArray(QString token,int id);
    void insertMusicToSongList(QString token,int music_id,int songList_id);
    void deleteMusicFromSongList(QString token,int music_id,int songList_id);
    void insertListenRecord(QString token);
    void downLoadMusic(QString url,QString musicName);
signals:
    void downInfoFinished(QByteArray array);

};

#endif // MUSICNETWORKIMP_H
