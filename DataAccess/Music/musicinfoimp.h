#ifndef MUSICINFOIMP_H
#define MUSICINFOIMP_H

#include <QObject>
#include "DataAccess/jsoninfointerface.h"
#include "Entity/music.h"
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonParseError>

class MusicInfoImp : public QObject,public JsonInfoInterface
{
    Q_OBJECT
public:
    explicit MusicInfoImp(QObject *parent = nullptr);
    void getInfo();
    QList<Music> getMusicList();
    int getUserSongList();
signals:

private:
     QList<Music> musicList;
};

#endif // MUSICINFOIMP_H
