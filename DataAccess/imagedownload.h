#ifndef IMAGEDOWNLOAD_H
#define IMAGEDOWNLOAD_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QFile>
#include <QHttpPart>

class ImageDownload : public QObject
{
    Q_OBJECT
public:
    explicit ImageDownload(QObject *parent = nullptr);
    void downImageFile();
    void setImageUrlList(QStringList);
    void postImageFile(QString path);
signals:
    void downLoadImageFinish();
    void postImageFileFinsh(QByteArray);
private:
    int index=0;
    QStringList imageList;
    QNetworkAccessManager *manager;
    QNetworkReply *reply;
};

#endif // IMAGEDOWNLOAD_H
