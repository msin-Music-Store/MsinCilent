#ifndef JSONINFOINTERFACE_H
#define JSONINFOINTERFACE_H

#include <QObject>

class JsonInfoInterface
{
public:
    JsonInfoInterface();
    virtual void getInfo()=0;
    void setArray(QByteArray newarray);
public:
    QByteArray array;
};

#endif // JSONINFOINTERFACE_H
