#ifndef NETWORKINTERFACE_H
#define NETWORKINTERFACE_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>


class NetworkInterface
{
public:
    NetworkInterface();
    virtual void setArray()=0;
    void setUrl(QString url);
    QByteArray getArray();
protected:
    QString url;
    QByteArray array;
    QNetworkAccessManager *manager;
    QNetworkReply *reply;
};

#endif // NETWORKINTERFACE_H
