#ifndef USERCONTROL_H
#define USERCONTROL_H

#include <QObject>
#include "DataAccess/User/userinfoimp.h"
#include "DataAccess/User/usernetworkimp.h"
#include "DataAccess/imagedownload.h"


class UserControl : public QObject
{
    Q_OBJECT
public:
    explicit UserControl(QObject *parent = nullptr);
    void getLogin(QString username,QString password);
    void getUser(QString token);
    void dealArray(QByteArray array);
    void getPostImage(QString);
    void updateUserInfo(User user,QString token);
    void registerUser(User user);
    void createMySongList(int id,QString name,QString token);
signals:
    void getLoginInfo(QStringList);
    void getUserInfo(User);
    void getPostImageUrl(QString);
    void getRegisterResult(bool);
private:
    int mode=0;
    UserInfoImp *userInfoImp=new UserInfoImp(this);
    UserNetworkImp *userNetworkImp=new UserNetworkImp(this);
    ImageDownload *image=new ImageDownload(this);
    User user;
};

#endif // USERCONTROL_H
