#include "musicplaycontrol.h"

MusicPlayControl::MusicPlayControl(QObject *parent) : QObject(parent)
{
    timer=new QTimer(this);
    isPlaying=false;
    player=new QMediaPlayer(this);
    playList=new QMediaPlaylist(this);
    player->setPlaylist(playList);
    playList->clear();
    connect(timer,&QTimer::timeout,this,&MusicPlayControl::sendPosition);
}
void MusicPlayControl::sendPosition()  //实时获取歌曲播放总时长与进度
{
    emit getPosition(player->position());
    emit getSongSize(player->duration());
}

void MusicPlayControl::play() //播放
{
   timer->start(2);
   player->play();
}
void MusicPlayControl::playByUrl(QString url) //播放
{
   timer->stop();
   int index=0;
   if(playList->mediaCount()==0)
   {
       playList->addMedia(QUrl(url));
       list.clear();
       list.append(url);
       index=0;
   }else
   {
        if(list.contains(url))
        {
            for(int i=0;i<list.size();i++)
            {
                if(list.at(i)==url)
                {
                    index=i;
                }
            }
        }else{
            playList->addMedia(QUrl(url));
            list.append(url);
            index=playList->mediaCount()-1;
        }
   }
   playList->setCurrentIndex(index);
   player->play();
   timer->start();
}
void MusicPlayControl::pause() //暂停
{
    timer->stop();
    player->pause();
}

void MusicPlayControl::setMusicSound(int value) //设置音量
{
    player->setVolume(value);
}

void MusicPlayControl::next()//下一首
{
    playList->next();
    player->play();
}
void MusicPlayControl::previous()//上一首
{
    playList->previous();
    player->play();
}

void MusicPlayControl::setPlayPosition(qint64 position)   //设置歌曲播放进度
{
    player->setPosition(position);
}

void MusicPlayControl::quit()  //退出播放器
{
    timer->stop();
    player->stop();
    playList->clear();
    delete timer;
    delete player;
    delete playList;
}

void MusicPlayControl::addSongtoPlayerList(QStringList urlList) // 添加歌曲进入播放列表
{
    for(QString url : urlList)
    {
        playList->addMedia(QUrl(url));
    }
}

void MusicPlayControl::setPlayMode(int index)   //设置播放模式
{
    if(index==1)
    {
        playList->setPlaybackMode(QMediaPlaylist::CurrentItemOnce);
    }else if(index==2){
        playList->setPlaybackMode(QMediaPlaylist::CurrentItemInLoop);
    }else if(index==3){
        playList->setPlaybackMode(QMediaPlaylist::Loop);
    }else if(index==4){
        playList->setPlaybackMode(QMediaPlaylist::Random);
    }
}

void MusicPlayControl::clearPlaylist()  //清空播放列表
{
    playList->clear();
}
