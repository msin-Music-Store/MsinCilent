#include "artistcontrol.h"

ArtistControl::ArtistControl(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<QList<Artist>>("QList<Artist>");
    qRegisterMetaType<Artist>("Artist");
    connect(artistNetworkImp,&ArtistNetworkImp::downInfoFinished,this,&ArtistControl::dealArtistInfoByNation);
    connect(image,&ImageDownload::downLoadImageFinish,this,&ArtistControl::imageDownResult);
}
void ArtistControl::getArtistInfoByNation(int id)
{
    mode=0;
    QString url=QString("http://127.0.0.1:5000/Artist/classification/%1").arg(id);
    artistNetworkImp->setUrl(url);
    artistNetworkImp->setArray();
}

void ArtistControl::getArtistInfoById(int id)
{
    mode=1;
    QString url=QString("http://127.0.0.1:5000/Artist/%1").arg(id);
    artistNetworkImp->setUrl(url);
    artistNetworkImp->setArray();
}

void ArtistControl::dealArtistInfoByNation(QByteArray array)
{
    if(mode==0)
    {
        QStringList imageList;
        artistList.clear();
        artistInfoImp->setArray(array);
        artistInfoImp->getInfo();
        for(Artist artist: artistInfoImp->getArtistListByclas())
        {
            artistList.append(artist);
            imageList.append(artist.getImage_url());
        }
        image->setImageUrlList(imageList);
        image->downImageFile();
    }else if(mode==1)
    {
        artistInfoImp->setArray(array);
        artistInfoImp->getInfoById();
        artist=artistInfoImp->getArtist();
        emit getArtistById(artist);
    }

}


void ArtistControl::imageDownResult()
{
    emit getArtistByNation(artistList);
}
