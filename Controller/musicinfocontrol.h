#ifndef MUSICINFOCONTROL_H
#define MUSICINFOCONTROL_H

#include <QObject>
#include "Entity/music.h"
#include "DataAccess/Music/musicnetworkimp.h"
#include "DataAccess/Music/musicinfoimp.h"

class MusicInfoControl : public QObject
{
    Q_OBJECT
public:
    explicit MusicInfoControl(QObject *parent = nullptr);
    void getAllMusic();
    void dealNetWorkSignal(QByteArray array);
    void getReommendMusic(QString token);
    void getUserSongList(QString token);
    void searchMusic(QString);
    void getMusicByartistId(int id);
    void getMusicByClas(int id);
    void insertMusicToSongList(int music_id);
    void deleteMusicFromList(int music_id);
    void insertListenRecord(QString,int music_id);
signals:
    void getAllMusicInfo(QList<Music>);
    void getSearchMusicInfo(QList<Music>);
    void getMusicByArtistId(QList<Music>);
    void getMusicInfoByClas(QList<Music>);
    void getRecommendMusicInfo(QList<Music>);
    void getCollectMusic(QList<Music>);
private:
    int mode=0;
    int songListId=0;
    QString token;
    MusicNetworkImp *musicNetworkImp=new MusicNetworkImp(this);
    MusicInfoImp *musicInfoImp=new MusicInfoImp(this);
};

#endif // MUSICINFOCONTROL_H
