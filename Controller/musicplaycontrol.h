#ifndef MUSICPLAYCONTROL_H
#define MUSICPLAYCONTROL_H

#include <QObject>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QTimer>
#include "Entity/music.h"

class MusicPlayControl : public QObject
{
    Q_OBJECT
public:
    explicit MusicPlayControl(QObject *parent = nullptr);
    void pause();
    void play();
    void playByUrl(QString url);
    void addSongtoPlayerList(QStringList urlList);
    void clearPlaylist();
    void quit();
    void sendPosition();
    void setMusicSound(int value);
    void setPlayPosition(qint64 position);
    void next();
    void setPlayMode(int index);   //设置歌曲播放模式
    void previous();
signals:
    void getPosition(qint64 position);
    void startSend();
    void getSongSize(qint64 size);
private:
    QMediaPlayer *player;
    QMediaPlaylist *playList;
    QStringList list;
    QTimer *timer;
    bool isPlaying;
};

#endif // MUSICPLAYCONTROL_H
