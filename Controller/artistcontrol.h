#ifndef ARTISTCONTROL_H
#define ARTISTCONTROL_H

#include <QObject>
#include "DataAccess/Artist/artistinfoimp.h"
#include "DataAccess/Artist/artistnetworkimp.h"
#include "DataAccess/imagedownload.h"

class ArtistControl : public QObject
{
    Q_OBJECT
public:
    explicit ArtistControl(QObject *parent = nullptr);
    void getArtistInfoByNation(int id);
    void getArtistInfoById(int id);
    void imageDownResult();
signals:
    void getArtistByNation(QList<Artist>);
    void getArtistById(Artist);
private:
    void dealArtistInfoByNation(QByteArray array);
    ImageDownload *image=new ImageDownload(this);
    ArtistInfoImp *artistInfoImp=new ArtistInfoImp(this);
    ArtistNetworkImp *artistNetworkImp=new ArtistNetworkImp(this);
    QList<Artist> artistList;
    Artist artist;
    int mode=0;
};

#endif // ARTISTCONTROL_H
