#include "usercontrol.h"

UserControl::UserControl(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<User>("User");
    connect(userNetworkImp,&UserNetworkImp::downLoadFinish,this,&UserControl::dealArray);
    connect(image,&ImageDownload::downLoadImageFinish,this,[=](){
        emit getUserInfo(user);
    });
    connect(image,&ImageDownload::postImageFileFinsh,this,[=](QByteArray array){
        QJsonParseError jsonError;
        QJsonDocument doucment = QJsonDocument::fromJson(array, &jsonError);  // 转化为 JSON 文档
        if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError)) {  // 解析未发生错误
           if(doucment.isArray())
           {
               QJsonArray A=doucment.array();
                for(int i=0;i<A.size();i++)
                {
                    emit getPostImageUrl(A.at(i).toString());
                }
           }
        }
    });
}
void UserControl::getLogin(QString username,QString password)
{
    mode=0;
    userNetworkImp->setUrl("http://127.0.0.1:5000/User/login");
    userNetworkImp->setLoginArray(username,password);
}
void UserControl::getUser(QString token)
{
    mode=1;
    userNetworkImp->setUrl("http://127.0.0.1:5000/User/info");
    userNetworkImp->setUserInfoArray(token);
}

void UserControl::updateUserInfo(User user,QString token)
{
    userNetworkImp->setUrl("http://127.0.0.1:5000/User/change");
    userNetworkImp->updateUser(user,token);
}
void UserControl::getPostImage(QString path)
{
    image->postImageFile(path);
}

void UserControl::registerUser(User user)
{
    mode=2;
    userNetworkImp->setUrl("http://127.0.0.1:5000/User/register");
    userNetworkImp->registerUser(user);
}

void UserControl::dealArray(QByteArray array)
{
    if(mode==0)
    {
        userInfoImp->setArray(array);
        emit getLoginInfo(userInfoImp->getLoginInfo());
    }
    if(mode==1)
    {
        QStringList imageList;
        user=userInfoImp->getUser();
        userInfoImp->setArray(array);
        userInfoImp->getInfo();
        QString str=QString("http://127.0.0.1:5000/Image%1").arg(user.getHeadUrl());
        imageList.append(str);
        image->setImageUrlList(imageList);
        image->downImageFile();

    }
    if(mode==2)
    {
        userInfoImp->setArray(array);
        emit getRegisterResult(userInfoImp->getRegisterInfo());
    }
}

void UserControl::createMySongList(int id,QString name,QString token)
{
    userNetworkImp->setUrl("http://127.0.0.1:5000/SongList");
    userNetworkImp->createMysongList(id,name,token);
}
