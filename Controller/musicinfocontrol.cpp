#include "musicinfocontrol.h"

MusicInfoControl::MusicInfoControl(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<QList<Music>>("QList<Music>");
    connect(musicNetworkImp,&MusicNetworkImp::downInfoFinished,this,&MusicInfoControl::dealNetWorkSignal);
}

void MusicInfoControl::getAllMusic()
{
    mode=0;
    musicNetworkImp->setUrl("http://127.0.0.1:5000/Music/all");
    musicNetworkImp->setArray();
}

void MusicInfoControl::searchMusic(QString s)
{
    mode=1;
    QString url=QString("http://127.0.0.1:5000/Music/search/%1").arg(s);
    musicNetworkImp->setUrl(url);
    musicNetworkImp->setSearchArray();
}

void MusicInfoControl::getMusicByartistId(int id)
{
    mode=2;
    QString url=QString("http://127.0.0.1:5000/Artist/%1/musics").arg(id);
    musicNetworkImp->setUrl(url);
    musicNetworkImp->setSearchArray();
}

void MusicInfoControl::getMusicByClas(int id)
{
    mode=3;
    QString url=QString("http://127.0.0.1:5000/Classification/%1/music").arg(id);
    musicNetworkImp->setUrl(url);
    musicNetworkImp->setSearchArray();
}

void MusicInfoControl::getReommendMusic(QString token)
{
    mode=4;
    QString url=QString("http://127.0.0.1:5000/Music/recommend");
    musicNetworkImp->setUrl(url);
    musicNetworkImp->setRecommendArray(token);
}

void MusicInfoControl::getUserSongList(QString newtoken)
{
    mode=5;
    token=newtoken;
    musicNetworkImp->setSongListArray(token);
}

void MusicInfoControl::insertMusicToSongList(int music_id)
{
    musicNetworkImp->insertMusicToSongList(token,music_id,songListId);
}

void MusicInfoControl::deleteMusicFromList(int music_id)
{
    musicNetworkImp->deleteMusicFromSongList(token,music_id,songListId);
}

void MusicInfoControl::insertListenRecord(QString newtoken,int music_id)
{
    token=newtoken;
    QString url=QString("http://127.0.0.1:5000/User/record/%1").arg(music_id);
    musicNetworkImp->setUrl(url);
    musicNetworkImp->insertListenRecord(token);
}

void MusicInfoControl::dealNetWorkSignal(QByteArray array)
{
    if(mode== 0)
    {
        musicInfoImp->setArray(array);
        musicInfoImp->getInfo();
        emit getAllMusicInfo(musicInfoImp->getMusicList());
    }
    if(mode== 1)
    {
        musicInfoImp->setArray(array);
        musicInfoImp->getInfo();
        emit getSearchMusicInfo(musicInfoImp->getMusicList());
    }

    if(mode== 2)
    {
        musicInfoImp->setArray(array);
        musicInfoImp->getInfo();
        emit getMusicByArtistId(musicInfoImp->getMusicList());
    }
    if(mode== 3)
    {
        musicInfoImp->setArray(array);
        musicInfoImp->getInfo();
        emit getMusicInfoByClas(musicInfoImp->getMusicList());
    }
    if(mode== 4)
    {
        musicInfoImp->setArray(array);
        musicInfoImp->getInfo();
        emit getRecommendMusicInfo(musicInfoImp->getMusicList());
    }if(mode== 5)
    {
        musicInfoImp->setArray(array);
        songListId=musicInfoImp->getUserSongList();
        mode=6;
        musicNetworkImp->setCollectArray(token,songListId);
    }if(mode== 6)
    {
        musicInfoImp->setArray(array);
        musicInfoImp->getInfo();
        emit getCollectMusic(musicInfoImp->getMusicList());
    }
}




