#include "artist.h"

Artist::Artist()
{

}

int Artist::getId() const
{
    return id;
}

void Artist::setId(int newId)
{
    id = newId;
}

const QString &Artist::getName() const
{
    return name;
}

void Artist::setName(const QString &newName)
{
    name = newName;
}

const QString &Artist::getSex() const
{
    return sex;
}

void Artist::setSex(const QString &newSex)
{
    sex = newSex;
}

const QString &Artist::getBirthday() const
{
    return birthday;
}

void Artist::setBirthday(const QString &newBirthday)
{
    birthday = newBirthday;
}

const QString &Artist::getNation() const
{
    return nation;
}

void Artist::setNation(const QString &newNation)
{
    nation = newNation;
}

const QString &Artist::getImage_url() const
{
    return image_url;
}

void Artist::setImage_url(const QString &newImage_url)
{
    image_url = newImage_url;
}
