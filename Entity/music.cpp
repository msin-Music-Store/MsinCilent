#include "music.h"

Music::Music()
{

}

QString Music::getMusicName()
{
    return this->musicName;
}
QString Music::getFileUrl()
{
    return this->fileUrl;
}
QString Music::getMusicSize()
{
    return this->musicSize;
}

int Music::getId()
{
    return this->id;
}

QString Music::getAlbumName()
{
    return this->albumName;
}
QString Music::getArtistName()
{
    return this->artistName;
}

QString Music::getclassificationName()
{
    return this->classificationName;
}

void Music::setClassificationName(QString classificationName)
{
    this->classificationName=classificationName;
}

void Music::setAlbumName(QString albumName)
{
    this->albumName=albumName;
}
void Music::setArtistName(QString artistName)
{
    this->artistName=artistName;
}

void Music::setId(int id)
{
    this->id=id;
}
void Music::setMusicName(QString musicName)
{
    this->musicName=musicName;
}
void Music::setFileUrl(QString fileUrl)
{
    this->fileUrl=fileUrl;
}
void Music::setMusicSize(QString musicSize)
{
    this->musicSize=musicSize;
}
