#include "user.h"

User::User()
{

}

User::User(QString username,QString password,QString headUrl,QString email,QString location)
{
    this->username=username;
    this->password=password;
    this->email=email;
    this->headUrl=headUrl;
    this->location=location;
}

void User::setUserName(QString username)
{
    this->username=username;
}
void User::setPassWord(QString password)
{
    this->password=password;
}
void User::setEmail(QString email)
{
    this->email=email;
}
void User::setHeadUrl(QString headUrl)
{
    this->headUrl=headUrl;
}
void User::setLocation(QString location)
{
    this->location=location;
}
void User::setToken(QString token)
{
    this->token=token;
}

void User::setId(int id)
{
    this->id=id;
}

QString User::getUserName()
{
    return this->username;
}
QString User::getPassWord()
{
    return this->password;
}
QString User::getEmail()
{
    return this->email;
}
QString User::getHeadUrl()
{
    return this->headUrl;
}
QString User::getLocation()
{
    return this->location;
}

QString User::getToken()
{
    return this->token;
}

int User::getId()
{
    return id;
}
