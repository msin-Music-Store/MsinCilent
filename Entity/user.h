#ifndef USER_H
#define USER_H

#include <QObject>
class User
{
public:
    User();
    User(QString username,QString password,QString headUrl,QString email="",QString location="");

    void setUserName(QString username);
    void setPassWord(QString password);
    void setEmail(QString email);
    void setHeadUrl(QString headUrl);
    void setLocation(QString location);
    void setToken(QString token);
    void setId(int id);

    QString getUserName();
    QString getPassWord();
    QString getEmail();
    QString getHeadUrl();
    QString getLocation();
    QString getToken();
    int getId();
private:
    QString username;
    QString password;
    QString email;
    QString headUrl;
    QString location;
    QString token;
    int id=0;
};

#endif // USER_H
