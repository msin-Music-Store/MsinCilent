#ifndef ARTIST_H
#define ARTIST_H

#include <QObject>
#include <QDateTime>

class Artist
{
public:
    Artist();

    int getId() const;
    void setId(int newId);
    const QString &getName() const;
    void setName(const QString &newName);
    const QString &getSex() const;
    void setSex(const QString &newSex);
    const QString &getBirthday() const;
    void setBirthday(const QString &newBirthday);
    const QString &getNation() const;
    void setNation(const QString &newNation);
    const QString &getImage_url() const;
    void setImage_url(const QString &newImage_url);

private:
    int id;
    QString name;
    QString sex;
    QString birthday;
    QString nation;
    QString image_url;
};

#endif // ARTIST_H
