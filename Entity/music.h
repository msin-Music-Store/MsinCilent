#ifndef MUSIC_H
#define MUSIC_H

#include <QObject>

class Music
{
public:
    Music();
    QString getMusicName();
    QString getFileUrl();
    QString getMusicSize();
    int getId();
    QString getAlbumName();
    QString getArtistName();
    QString getclassificationName();

    void setClassificationName(QString classificationName);
    void setAlbumName(QString albumName);
    void setArtistName(QString artistName);
    void setId(int id);
    void setMusicName(QString musicName);
    void setFileUrl(QString fileUrl);
    void setMusicSize(QString musicSize);

private:
    int id;
    QString classificationName;
    QString albumName;
    QString artistName;
    QString musicName;
    QString fileUrl;
    QString musicSize;
};

#endif // MUSIC_H
