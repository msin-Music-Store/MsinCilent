#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include "Controller/musicinfocontrol.h"
#include "Controller/musicplaycontrol.h"
#include "Controller/artistcontrol.h"
#include <QThread>
#include <QDir>
#include <QStandardItemModel>
#include <QMenu>
#include <QMessageBox>
#include <QListWidgetItem>
#include <QFileDialog>
#include <QFileInfo>
#include "UI/logindialog.h"
#include <QPoint>
#include <QTimer>
namespace Ui {
class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setPlayingSilder(qint64);
    void setPlayingDuration(qint64);

    void initRecommendView(QList<Music>);
    void initSearchMusicView(QList<Music>);

    void createLocalStore();
    void initArtistByNationView(QList<Artist>);
    void initArtistInfoView(Artist);
    void initArtistMusicView(QList<Music>);
    void initClasMusicView(QList<Music>);
    void initMySongListView(QList<Music>);

    void initUserInfo(User,QString);
    void getPostImageUrl(QString url);

    void deleteThread();
signals:
    void getAllMusic();
    void getRecommendMusuic(QString token);
    void getMusicByArtistId(int id);
    void getMusicByClas(int id);
    void getSongListMusic(QString token);
    void addMusicToPlayList(QStringList list);
    void insertMusicToSongList(int music_id);
    void deleteMusicFromList(int music_id);
    void insertListenRecord(QString token,int music_id);

    void updateUser(User,QString token);

    void postImage(QString url);

    void play();
    void playByUrl(QString);
    void previous();
    void next();
    void pause();
    void searchMusic(QString);
    void setVolum(int volum);
    void setPlayMode(int mode);
    void setPlayPosition(qint64 position);

    void getArtistByNation(int id);
    void getArtistById(int id);
private slots:
    void on_recommedView_doubleClicked(const QModelIndex &index);

    void on_btnPrevious_clicked();

    void on_btnNext_clicked();

    void on_btnPlaying_clicked();

    void on_recommedView_customContextMenuRequested(const QPoint &pos);

    void on_btnSearch_clicked();

    void on_btnSeacrhBack_clicked();

    void on_searchMusicView_doubleClicked(const QModelIndex &index);

    void on_searchMusicView_customContextMenuRequested(const QPoint &pos);

    void on_DlistWidget_currentRowChanged(int currentRow);

    void on_GlistWidget_currentRowChanged(int currentRow);

    void on_RlistWidget_currentRowChanged(int currentRow);

    void on_OlistWidget_currentRowChanged(int currentRow);

    void on_artistImageListWidget_itemClicked(QListWidgetItem *item);

    void on_artistMusicView_doubleClicked(const QModelIndex &index);

    void on_artistMusicView_customContextMenuRequested(const QPoint &pos);

    void on_volumSilder_valueChanged(int value);

    QString transTime(qint64 ms);

    void on_btnMute_clicked();

    void on_clasListWidget_currentRowChanged(int currentRow);

    void on_tabWidget_currentChanged(int index);

    void on_clasMusicView_doubleClicked(const QModelIndex &index);

    void on_clasMusicView_customContextMenuRequested(const QPoint &pos);

    void on_btnArtistInfoBack_clicked();

    void on_btnLocalFile_clicked();

    void on_btnLocal_clicked();

    void on_localMusicView_doubleClicked(const QModelIndex &index);

    void on_btnMain_clicked();

    void on_comboBox_currentIndexChanged(const QString &arg1);

    void on_btnUserInfoBack_clicked();

    void on_btnCancle_clicked();

    void on_btnMySongList_clicked();

    void on_songListMusicView_customContextMenuRequested(const QPoint &pos);

    void on_songListMusicView_doubleClicked(const QModelIndex &index);

    void on_btnPlayMode_clicked();

    void on_btnChangeHead_clicked();

    void on_btnChange_clicked();

    void on_btnChangePasswordBack_clicked();

    void on_btnCanclechangePassword_clicked();

    void on_btnChangePassword_clicked();
    void initPlayView(QList<Music> list);

    void on_playView_doubleClicked(const QModelIndex &index);

    void on_playView_customContextMenuRequested(const QPoint &pos);
    void scrollCaption();

    void on_playingPosition_sliderMoved(int position);

private:
    Ui::MainWindow *ui;

    bool isPlaying=false;
    bool isMute=false;
    bool isLogin=false;

    int playMode=1;

    User user;
    QString token;

    LoginDialog *loginDialog;

    QStringList localList;

    int index=0;
    QList<Music> playList;
    QStandardItemModel* playItem = new QStandardItemModel(this);

    QString userfilePath;
    QString changefilePath;

    QList<Artist> artistList;
    QStandardItemModel* localMusicItem = new QStandardItemModel(this);

    QList<Music> recommendMusicList;
    QStandardItemModel* recommendMusicItem = new QStandardItemModel(this);

    QList<Music> songlistMusicList;
    QStandardItemModel* songlistMusicItem = new QStandardItemModel(this);

    QList<Music> artistMusicList;
    QStandardItemModel* artistMusicItem = new QStandardItemModel(this);

    QList<Music> clasMusicList;
    QStandardItemModel* clasMusicItem = new QStandardItemModel(this);

    QList<Music> searchMusicList;
    QStandardItemModel* searchMusicItem = new QStandardItemModel(this);

    QThread *musicPlayThread=new QThread(this);
    MusicPlayControl *musicplay=new MusicPlayControl();

    QThread *userThread=new QThread(this);
    UserControl *userControl=new UserControl();

    QThread *musicInfoThread=new QThread(this);
    MusicInfoControl *musicInfo=new MusicInfoControl();

    QThread *artistInfoThread=new QThread(this);
    ArtistControl *artistInfo=new ArtistControl();

    QTimer *timer;
    QString isPlayingName="";
};

#endif // MAINWINDOW_H
