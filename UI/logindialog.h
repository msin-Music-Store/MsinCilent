#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include <QThread>
#include "Controller/usercontrol.h"
#include <QMessageBox>
#include "UI/registerdialog.h"

namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoginDialog(QWidget *parent = nullptr);
    ~LoginDialog();
    void deleteThread();
private slots:
    void on_btnLogin_clicked();
    void getLoginInfo(QStringList);
    void getUserInfo(User);
    void on_btnRegister_clicked();

signals:
    void login(QString username,QString password);
    void getUser(QString token);
    void loginSucceed(User,QString);
private:
    Ui::LoginDialog *ui;
    QThread *userThread=new QThread(this);
    UserControl *userControl=new UserControl();
    QString token;
    User user;
    RegisterDialog *registerDialog;
};

#endif // LOGINDIALOG_H
