#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    loginDialog=new LoginDialog(this);
    timer=new QTimer(this);
    this->setWindowTitle("MusicPlayer");
    this->setWindowIcon(QIcon(":/images/MusicPlayer.png"));
    QPixmap pix(":/images/headboy.png");
    pix=pix.scaled(35,35);
    ui->headPhoto->setPixmap(pix);
    ui->btnMySongList->setVisible(false);

    ui->stackedWidget->setCurrentIndex(0);
    ui->tabWidget->setCurrentIndex(0);
    ui->DlistWidget->setCurrentRow(0);
    ui->toolBox->setCurrentIndex(0);
    ui->volumSilder->setValue(50);

    createLocalStore();

    musicInfo->moveToThread(musicInfoThread);
    musicplay->moveToThread(musicPlayThread);
    artistInfo->moveToThread(artistInfoThread);
    userControl->moveToThread(userThread);

    userThread->start();
    musicInfoThread->start();
    musicPlayThread->start();
    artistInfoThread->start();

    connect(this,&MainWindow::getAllMusic,musicInfo,&MusicInfoControl::getAllMusic);
    connect(musicInfo,&MusicInfoControl::getAllMusicInfo,this,&MainWindow::initRecommendView);
    connect(this,&MainWindow::searchMusic,musicInfo,&MusicInfoControl::searchMusic);
    connect(musicInfo,&MusicInfoControl::getSearchMusicInfo,this,&MainWindow::initSearchMusicView);
    connect(this,&MainWindow::getMusicByArtistId,musicInfo,&MusicInfoControl::getMusicByartistId);
    connect(musicInfo,&MusicInfoControl::getMusicByArtistId,this,&MainWindow::initArtistMusicView);
    connect(this,&MainWindow::getMusicByClas,musicInfo,&MusicInfoControl::getMusicByClas);
    connect(musicInfo,&MusicInfoControl::getMusicInfoByClas,this,&MainWindow::initClasMusicView);
    connect(this,&MainWindow::getRecommendMusuic,musicInfo,&MusicInfoControl::getReommendMusic);
    connect(musicInfo,&MusicInfoControl::getRecommendMusicInfo,this,&MainWindow::initRecommendView);
    connect(this,&MainWindow::getSongListMusic,musicInfo,&MusicInfoControl::getUserSongList);
    connect(musicInfo,&MusicInfoControl::getCollectMusic,this,&MainWindow::initMySongListView);
    connect(this,&MainWindow::insertMusicToSongList,musicInfo,&MusicInfoControl::insertMusicToSongList);
    connect(this,&MainWindow::deleteMusicFromList,musicInfo,&MusicInfoControl::deleteMusicFromList);
    connect(this,&MainWindow::insertListenRecord,musicInfo,&MusicInfoControl::insertListenRecord);

    connect(this,&MainWindow::addMusicToPlayList,musicplay,&MusicPlayControl::addSongtoPlayerList);
    connect(this,&MainWindow::play,musicplay,&MusicPlayControl::play);
    connect(this,&MainWindow::playByUrl,musicplay,&MusicPlayControl::playByUrl);
    connect(this,&MainWindow::previous,musicplay,&MusicPlayControl::previous);
    connect(this,&MainWindow::next,musicplay,&MusicPlayControl::next);
    connect(this,&MainWindow::pause,musicplay,&MusicPlayControl::pause);
    connect(this,&MainWindow::setVolum,musicplay,&MusicPlayControl::setMusicSound);
    connect(musicplay,&MusicPlayControl::getSongSize,this,&MainWindow::setPlayingDuration);
    connect(musicplay,&MusicPlayControl::getPosition,this,&MainWindow::setPlayingSilder);
    connect(this,&MainWindow::setPlayMode,musicplay,&MusicPlayControl::setPlayMode);
    connect(this,&MainWindow::setPlayPosition,musicplay,&MusicPlayControl::setPlayPosition);

    connect(this,&MainWindow::getArtistByNation,artistInfo,&ArtistControl::getArtistInfoByNation);
    connect(artistInfo,&ArtistControl::getArtistByNation,this,&MainWindow::initArtistByNationView);
    connect(this,&MainWindow::getArtistById,artistInfo,&ArtistControl::getArtistInfoById);
    connect(artistInfo,&ArtistControl::getArtistById,this,&MainWindow::initArtistInfoView);

    connect(this,&MainWindow::postImage,userControl,&UserControl::getPostImage);
    connect(userControl,&UserControl::getPostImageUrl,this,&MainWindow::getPostImageUrl);
    connect(this,&MainWindow::updateUser,userControl,&UserControl::updateUserInfo);

    connect(loginDialog,&LoginDialog::loginSucceed,this,&MainWindow::initUserInfo);

    connect(this,&MainWindow::destroyed,this,&MainWindow::deleteThread);
    connect(this,&MainWindow::destroyed,musicplay,&MusicPlayControl::quit);

    connect(timer, &QTimer::timeout, this, &MainWindow::scrollCaption);

    emit getAllMusic();
}

//定时器事件，设置字幕滚动
void MainWindow::scrollCaption()
{
    static int nPos = 0;
    //当截取的位置比字符串长时，从头开始
    if (nPos > isPlayingName.length())
        nPos = 0;
   //设置控件文本
   ui->isPlayingMusicName->setText(isPlayingName.mid(nPos));
    nPos++;
}

void MainWindow::initRecommendView(QList<Music> list)  //初始化歌曲推荐页
{
    recommendMusicList.clear();
    recommendMusicItem->clear();
    recommendMusicItem->setColumnCount(4);
    recommendMusicItem->setHeaderData(0,Qt::Horizontal, "音乐标题");
    recommendMusicItem->setHeaderData(1,Qt::Horizontal, "歌手");
    recommendMusicItem->setHeaderData(2,Qt::Horizontal, "专辑");
    recommendMusicItem->setHeaderData(3,Qt::Horizontal, "时长");
    for (int i=0;i<list.size();i++ ) {
        Music music=list.at(i);
        this->recommendMusicList.append(music);
        this->recommendMusicItem->setItem(i,0, new QStandardItem(music.getMusicName()));
        this->recommendMusicItem->setItem(i,1, new QStandardItem(music.getArtistName()) );
        this->recommendMusicItem->setItem(i,2, new QStandardItem(music.getAlbumName()) );
        this->recommendMusicItem->setItem(i,3, new QStandardItem(music.getMusicSize()) );
        this->recommendMusicItem->item(i, 0)->setTextAlignment(Qt::AlignCenter);
        this->recommendMusicItem->item(i, 1)->setTextAlignment(Qt::AlignCenter);
        this->recommendMusicItem->item(i, 2)->setTextAlignment(Qt::AlignCenter);
        this->recommendMusicItem->item(i, 3)->setTextAlignment(Qt::AlignCenter);
    }
    ui->recommedView->setModel(recommendMusicItem);
    ui->recommedView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);//平均分配列宽
    ui->recommedView->setEditTriggers(QAbstractItemView::NoEditTriggers);  //不可编辑
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::deleteThread()   //销毁线程
{
    musicInfoThread->quit();
    musicInfoThread->wait(10);
    musicPlayThread->quit();
    musicPlayThread->wait(20);
    artistInfoThread->quit();
    artistInfoThread->wait(10);
    userThread->quit();
    userThread->wait(10);

    loginDialog->deleteThread();
    delete musicInfo;
    delete musicplay;
    delete artistInfo;
    delete loginDialog;
}

void MainWindow::createLocalStore()  //创建本地资源文件夹
{
    QDir *photo = new QDir();
   bool exist = photo->exists("C://Users//Public//MusicPlayer//Image");
   if(exist)
   {
       qDebug()<<"文件夹已存在！";
   } else
   {
        //创建photo文件夹
       qDebug()<<photo->mkpath("C://Users//Public//MusicPlayer//Image");
   }
   exist = photo->exists("C://Users//Public//MusicPlayer//Music");
   if(exist)
   {
       qDebug()<<"文件夹已存在！";
   } else
   {
        //创建Music文件夹
       qDebug()<<photo->mkpath("C://Users//Public//MusicPlayer//Music");
   }
}
MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_recommedView_doubleClicked(const QModelIndex &index)  //歌曲推荐页双击播放
{

    Music music=recommendMusicList.at(index.row());
    QPixmap pix(":/images/pause.png");
    pix=pix.scaled(22,22);
    ui->btnPlaying->setIcon(QIcon(":/images/pause.png"));
    isPlaying=true;
    isPlayingName=QString("%1-%2").arg(music.getMusicName()).arg(music.getArtistName());
    timer->start(500);
    emit playByUrl(music.getFileUrl());
    if(isLogin)
    {
        emit insertListenRecord(token,music.getId());
    }
    if(playList.isEmpty())
        {
            playList.append(music);
            this->index=0;
        }else{
            int nowindex=0;
            for(Music amusic : playList)
            {
                nowindex++;
                if(amusic.getMusicName()==music.getMusicName())
                {
                    this->index=nowindex;
                    return;
                }
            }
            playList.append(music);
            this->index=playList.size()-1;
        }
}

void MainWindow::on_btnPrevious_clicked()  //上一首
{
    if(this->index==0)
    {
        index=playList.size()-1;
    }else{
        index--;
    }
    Music music =playList.at(index);
    ui->isPlayingMusicName->setText(QString("%1-%2").arg(music.getMusicName()).arg(music.getArtistName()));
    emit previous();
}

void MainWindow::on_btnNext_clicked()  //下一首
{

    if(this->index==playList.size()-1)
    {
        index=0;

    }else{
        index++;
    }
    Music music =playList.at(index);
    ui->isPlayingMusicName->setText(QString("%1-%2").arg(music.getMusicName()).arg(music.getArtistName()));
    emit next();
}

void MainWindow::on_btnPlaying_clicked()  //播放暂停
{
    if(isPlaying)
    {
        isPlaying=false;
        timer->start(500);
        ui->btnPlaying->setIcon(QIcon(":/images/playing4.png"));
        emit pause();
    }else{
        isPlaying=true;
        timer->stop();
        ui->btnPlaying->setIcon(QIcon(":/images/pause.png"));
        emit play();
    }
}

void MainWindow::on_recommedView_customContextMenuRequested(const QPoint &pos)  //歌曲推荐页右键菜单
{
    QMenu *cmenu = new QMenu(ui->recommedView);

    QAction *action1 = new QAction(tr("收藏歌曲"), this);
    QAction *action2 = new QAction(tr("下载歌曲"), this);
    cmenu->resize(50,30);
    action1->setData(1);
    cmenu->addAction(action1);
    cmenu->addAction(action2);
    // 下面这个on_menu_click(bool)槽函数做自己想做的事
    connect(action1, &QAction::triggered, this,[=](bool s){
        if(!s){
            if(isLogin==false)
            {
                QMessageBox::information(this,"提示","请先登录再进行收藏歌曲!");
                return;
            }else{
                Music rmusic=recommendMusicList.at(ui->recommedView->currentIndex().row());
                for(Music music : songlistMusicList)
                {

                    if(music.getId()==rmusic.getId())
                    {
                        QMessageBox::information(this,"提示","该歌曲已在您的歌单中，请勿重复收藏!");
                        return;
                    }
                }
                emit insertMusicToSongList(rmusic.getId());
                QMessageBox::information(this,"提示","收藏成功❥(^_-)!");
            }
        }
    });
     connect(action2, &QAction::triggered, this,[=](bool s){
            Music rmusic=recommendMusicList.at(ui->recommedView->currentIndex().row());
            qDebug()<<rmusic.getMusicName();
            MusicNetworkImp *m=new MusicNetworkImp();
            m->downLoadMusic(rmusic.getFileUrl(),rmusic.getMusicName());
            QMessageBox::information(this,"提示","下载成功");
     });
    cmenu->exec(QCursor::pos());
}

void MainWindow::on_btnSearch_clicked()   //歌曲搜索
{
    if(ui->musicSearchEdit->text().isEmpty())
    {
        QMessageBox::information(this,"提示","搜索关键词为空");
    }else
    {
        emit searchMusic(ui->musicSearchEdit->text());
        ui->stackedWidget->setCurrentIndex(1);
    }
}

void MainWindow::initSearchMusicView(QList<Music> list)  //歌曲搜索结果页面
{
    searchMusicList.clear();
    searchMusicItem->clear();
    searchMusicItem->setColumnCount(4);
    searchMusicItem->setHeaderData(0,Qt::Horizontal, "音乐标题");
    searchMusicItem->setHeaderData(1,Qt::Horizontal, "歌手");
    searchMusicItem->setHeaderData(2,Qt::Horizontal, "专辑");
    searchMusicItem->setHeaderData(3,Qt::Horizontal, "时长");
    if(list.size()==0)
    {
        QMessageBox::information(this,"提示","搜索结果为空");
    }else{

        for (int i=0;i<list.size();i++ ) {
            Music music=list.at(i);
            this->searchMusicList.append(music);
            this->searchMusicItem->setItem(i,0, new QStandardItem(music.getMusicName()));
            this->searchMusicItem->setItem(i,1, new QStandardItem(music.getArtistName()) );
            this->searchMusicItem->setItem(i,2, new QStandardItem(music.getAlbumName()) );
            this->searchMusicItem->setItem(i,3, new QStandardItem(music.getMusicSize()) );
            this->searchMusicItem->item(i, 0)->setTextAlignment(Qt::AlignCenter);
            this->searchMusicItem->item(i, 1)->setTextAlignment(Qt::AlignCenter);
            this->searchMusicItem->item(i, 2)->setTextAlignment(Qt::AlignCenter);
            this->searchMusicItem->item(i, 3)->setTextAlignment(Qt::AlignCenter);
        }
        ui->searchMusicView->setModel(searchMusicItem);
        ui->searchMusicView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);//平均分配列宽
        ui->searchMusicView->setEditTriggers(QAbstractItemView::NoEditTriggers);  //不可编辑
    }
}

void MainWindow::on_btnSeacrhBack_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_searchMusicView_doubleClicked(const QModelIndex &index)  //歌曲搜索页双击播放
{
    Music music=searchMusicList.at(index.row());
    QPixmap pix(":/images/pause.png");
    pix=pix.scaled(22,22);
    ui->btnPlaying->setIcon(QIcon(":/images/pause.png"));
    isPlaying=true;
    isPlayingName=QString("%1-%2").arg(music.getMusicName()).arg(music.getArtistName());
    timer->start(500);
    emit playByUrl(music.getFileUrl());
    if(isLogin)
    {
        emit insertListenRecord(token,music.getId());
    }
    if(playList.isEmpty())
        {
            playList.append(music);
            this->index=0;
        }else{
            int noeindex=0;
            for(Music amusic : playList)
            {
                noeindex++;
                if(amusic.getMusicName()==music.getMusicName())
                {
                    this->index=noeindex;
                    return;
                }
            }
            playList.append(music);
            this->index=playList.size()-1;
        }
}

void MainWindow::on_searchMusicView_customContextMenuRequested(const QPoint &pos)  //歌曲搜索页右键菜单
{
    QMenu *cmenu = new QMenu(ui->searchMusicView);
    QAction *action1 = new QAction(tr("收藏歌曲"), this);
    QAction *action2 = new QAction(tr("下载歌曲"), this);
    cmenu->resize(50,30);
    action1->setData(1);
    cmenu->addAction(action1);
    cmenu->addAction(action2);
    // 下面这个on_menu_click(bool)槽函数做自己想做的事
    connect(action1, &QAction::triggered, this,[=](bool s){
        if(!s){
            if(isLogin==false)
            {
                QMessageBox::information(this,"提示","请先登录再进行收藏歌曲!");
                return;
            }else{
                Music rmusic=searchMusicList.at(ui->searchMusicView->currentIndex().row());
                for(Music music : songlistMusicList)
                {

                    if(music.getId()==rmusic.getId())
                    {
                        QMessageBox::information(this,"提示","该歌曲已在您的歌单中，请勿重复收藏!");
                        return;
                    }
                }
                emit insertMusicToSongList(rmusic.getId());
                QMessageBox::information(this,"提示","收藏成功❥(^_-)!");
            }
        }
    });

    connect(action2, &QAction::triggered, this,[=](bool s){
           Music rmusic=recommendMusicList.at(ui->recommedView->currentIndex().row());
           qDebug()<<rmusic.getMusicName();
           MusicNetworkImp *m=new MusicNetworkImp();
           m->downLoadMusic(rmusic.getFileUrl(),rmusic.getMusicName());
           QMessageBox::information(this,"提示","下载成功");
    });
    cmenu->exec(QCursor::pos());
}

void MainWindow::initArtistByNationView(QList<Artist> list)    //歌手按国籍地区浏览页
{
    artistList.clear();
    ui->artistImageListWidget->clear();
    int i=0;
    foreach(Artist artist,list)
    {
        artistList.append(artist);
        QString str="http://127.0.0.1:5000/Image/";
        QString fileName=artist.getImage_url().mid(str.length()-1);
        QString path=QString("C://Users//Public//MusicPlayer//Image//%1").arg(fileName);
        QPixmap pixmap(path);
        QListWidgetItem *item = new QListWidgetItem;
        QWidget *widget = new QWidget;
        QVBoxLayout *widgetLayout = new QVBoxLayout;
        QLabel *imageLabel = new QLabel;

        QLabel *txtLabel = new QLabel(tr("          %1").arg(artist.getName()));
        widget->setLayout(widgetLayout);
        widgetLayout->setMargin(0);
        widgetLayout->setSpacing(10);
        widgetLayout->addWidget(imageLabel);
        widgetLayout->addWidget(txtLabel);
        pixmap=pixmap.scaled(150,150);
        imageLabel->setPixmap(pixmap);
        txtLabel->setWordWrap(true);

        item->setSizeHint(QSize(150,170));
        item->setText(QString::number(i));
        ui->artistImageListWidget->addItem(item);
        ui->artistImageListWidget->setSizeIncrement(150,170);
        ui->artistImageListWidget->setItemWidget(item,widget);
        i++;
    }
}

void MainWindow::on_DlistWidget_currentRowChanged(int currentRow)  //大陆歌手选项卡
{
    if(currentRow==0)
    {
        emit getArtistByNation(1);
    }else if(currentRow==1){
        emit getArtistByNation(5);
    }
}

void MainWindow::on_GlistWidget_currentRowChanged(int currentRow)  //港台歌手选项卡
{
    if(currentRow==0)
    {
        emit getArtistByNation(2);
    }else if(currentRow==1){
        emit getArtistByNation(6);
    }
}

void MainWindow::on_RlistWidget_currentRowChanged(int currentRow)  //日韩歌手选项卡
{
    if(currentRow==0)
    {
        emit getArtistByNation(4);
    }else if(currentRow==1){
        emit getArtistByNation(8);
    }
}

void MainWindow::on_OlistWidget_currentRowChanged(int currentRow)  //欧美歌手选项卡
{
    if(currentRow==0)
    {
        emit getArtistByNation(3);
    }else if(currentRow==1){
        emit getArtistByNation(7);
    }
}

void MainWindow::on_artistImageListWidget_itemClicked(QListWidgetItem *item)  // 歌手图片点击
{
    qDebug()<<item->text().toInt();
    emit getArtistById(artistList.at(item->text().toInt()).getId());
    emit getMusicByArtistId(artistList.at(item->text().toInt()).getId());
    ui->stackedWidget->setCurrentIndex(2);
}

void MainWindow::initArtistInfoView(Artist artist) //歌手个人信息展示页
{
    ui->artistNameInfo->setText(artist.getName());
    ui->artistSexInfo->setText(artist.getSex());
    ui->artistNationInfo->setText(artist.getNation());
    ui->artistBirthdayInfo->setText(artist.getBirthday());
    QString str="http://127.0.0.1:5000/Image/";
    QString fileName=artist.getImage_url().mid(str.length()-1);
    QString path=QString("C://Users//Public//MusicPlayer//Image//%1").arg(fileName);
    QPixmap pixmap(path);
    pixmap=pixmap.scaled(120,120);
    ui->artistInfoHeadPhoto->setPixmap(pixmap);
}
void MainWindow::initArtistMusicView(QList<Music> list)  //歌手个人歌曲展示表
{
    artistMusicList.clear();
    artistMusicItem->clear();
    artistMusicItem->setColumnCount(4);
    artistMusicItem->setHeaderData(0,Qt::Horizontal, "音乐标题");
    artistMusicItem->setHeaderData(1,Qt::Horizontal, "歌手");
    artistMusicItem->setHeaderData(2,Qt::Horizontal, "专辑");
    artistMusicItem->setHeaderData(3,Qt::Horizontal, "时长");
    for (int i=0;i<list.size();i++ ) {
        Music music=list.at(i);
        this->artistMusicList.append(music);
        this->artistMusicItem->setItem(i,0, new QStandardItem(music.getMusicName()));
        this->artistMusicItem->setItem(i,1, new QStandardItem(music.getArtistName()) );
        this->artistMusicItem->setItem(i,2, new QStandardItem(music.getAlbumName()) );
        this->artistMusicItem->setItem(i,3, new QStandardItem(music.getMusicSize()) );
        this->artistMusicItem->item(i, 0)->setTextAlignment(Qt::AlignCenter);
        this->artistMusicItem->item(i, 1)->setTextAlignment(Qt::AlignCenter);
        this->artistMusicItem->item(i, 2)->setTextAlignment(Qt::AlignCenter);
        this->artistMusicItem->item(i, 3)->setTextAlignment(Qt::AlignCenter);
    }
    ui->artistMusicView->setModel(artistMusicItem);
    ui->artistMusicView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);//平均分配列宽
    ui->artistMusicView->setEditTriggers(QAbstractItemView::NoEditTriggers);  //不可编辑
}

void MainWindow::on_artistMusicView_doubleClicked(const QModelIndex &index)  //歌手个人歌曲双击播放
{
    Music music=artistMusicList.at(index.row());
    QPixmap pix(":/images/pause.png");
    pix=pix.scaled(22,22);
    ui->btnPlaying->setIcon(QIcon(":/images/pause.png"));
    isPlaying=true;
    isPlayingName=QString("%1-%2").arg(music.getMusicName()).arg(music.getArtistName());
    timer->start(500);
    emit playByUrl(music.getFileUrl());
    if(isLogin)
    {
        emit insertListenRecord(token,music.getId());
        this->index=0;
    }

    if(playList.isEmpty())
    {
        playList.append(music);
    }else{
        int nowindex=0;
        for(Music amusic : playList)
        {
            nowindex++;
            if(amusic.getMusicName()==music.getMusicName())
            {
                this->index=nowindex;
                return;
            }
        }
        playList.append(music);
        this->index=playList.size()-1;
    }

}

void MainWindow::on_artistMusicView_customContextMenuRequested(const QPoint &pos)  //歌手个人歌曲右键菜单
{
    QMenu *cmenu = new QMenu(ui->artistMusicView);
    QAction *action1 = new QAction(tr("收藏歌曲"), this);
    QAction *action2 = new QAction(tr("下载歌曲"), this);
    cmenu->resize(50,30);
    action1->setData(1);
    cmenu->addAction(action1);
    cmenu->addAction(action2);
    // 下面这个on_menu_click(bool)槽函数做自己想做的事
    connect(action1, &QAction::triggered, this,[=](bool s){
        if(!s){
            if(isLogin==false)
            {
                QMessageBox::information(this,"提示","请先登录再进行收藏歌曲!");
                return;
            }else{
                Music rmusic=artistMusicList.at(ui->artistMusicView->currentIndex().row());
                for(Music music : songlistMusicList)
                {

                    if(music.getId()==rmusic.getId())
                    {
                        QMessageBox::information(this,"提示","该歌曲已在您的歌单中，请勿重复收藏!");
                        return;
                    }
                }
                emit insertMusicToSongList(rmusic.getId());
                QMessageBox::information(this,"提示","收藏成功❥(^_-)!");
            }
        }
    });

    connect(action2, &QAction::triggered, this,[=](bool s){
           Music rmusic=recommendMusicList.at(ui->recommedView->currentIndex().row());
           qDebug()<<rmusic.getMusicName();
           MusicNetworkImp *m=new MusicNetworkImp();
           m->downLoadMusic(rmusic.getFileUrl(),rmusic.getMusicName());
           QMessageBox::information(this,"提示","下载成功");
    });
    cmenu->exec(QCursor::pos());
}

void MainWindow::on_volumSilder_valueChanged(int value)  //改变音量大小
{
    emit setVolum(value);
}

QString MainWindow::transTime(qint64 ms)   //将毫秒值转换成时间
{
    QDateTime time = QDateTime::fromMSecsSinceEpoch(ms); //时间戳-毫秒级
    QString strStartTime = time.toString("mm:ss");
    return strStartTime;
}

void MainWindow::setPlayingSilder(qint64 s)  //实时更新进度条实时更新播放时间
{
    ui->playingPosition->setValue(s);
    ui->playingTime->setText(transTime(s));
}
void MainWindow::setPlayingDuration(qint64 s)   //获取歌曲播放总时长以及设置时间
{
    ui->playingPosition->setMinimum(0);
    ui->playingPosition->setMaximum(s);
    ui->endingTime->setText(transTime(s));
}

void MainWindow::on_btnMute_clicked()   //设置静音
{
    if(isMute)
    {
        isMute=false;
        ui->btnMute->setIcon((QIcon(":/images/sound.png")));
        ui->volumSilder->setValue(50);
        emit setVolum(50);
    }else{
        isMute=true;
        ui->btnMute->setIcon((QIcon(":/images/soundPause.png")));
        ui->volumSilder->setValue(0);
        emit setVolum(0);
    }
}

void MainWindow::on_clasListWidget_currentRowChanged(int currentRow)  //根据歌曲曲风选择播放歌曲
{
    if(currentRow==0)
    {
        emit getMusicByClas(1);
    }
    if(currentRow==1)
    {
        emit getMusicByClas(2);
    }
    if(currentRow==2)
    {
        emit getMusicByClas(3);
    }
    if(currentRow==3)
    {
        emit getMusicByClas(4);
    }
    if(currentRow==4)
    {
        emit getMusicByClas(5);
    }
}

void MainWindow::initClasMusicView(QList<Music> list)   //初始化曲风选择歌曲页面
{
    clasMusicList.clear();
    clasMusicItem->clear();
    clasMusicItem->setColumnCount(4);
    clasMusicItem->setHeaderData(0,Qt::Horizontal, "音乐标题");
    clasMusicItem->setHeaderData(1,Qt::Horizontal, "歌手");
    clasMusicItem->setHeaderData(2,Qt::Horizontal, "专辑");
    clasMusicItem->setHeaderData(3,Qt::Horizontal, "时长");
    for (int i=0;i<list.size();i++ ) {
        Music music=list.at(i);
        this->clasMusicList.append(music);
        this->clasMusicItem->setItem(i,0, new QStandardItem(music.getMusicName()));
        this->clasMusicItem->setItem(i,1, new QStandardItem(music.getArtistName()) );
        this->clasMusicItem->setItem(i,2, new QStandardItem(music.getAlbumName()) );
        this->clasMusicItem->setItem(i,3, new QStandardItem(music.getMusicSize()) );
        this->clasMusicItem->item(i, 0)->setTextAlignment(Qt::AlignCenter);
        this->clasMusicItem->item(i, 1)->setTextAlignment(Qt::AlignCenter);
        this->clasMusicItem->item(i, 2)->setTextAlignment(Qt::AlignCenter);
        this->clasMusicItem->item(i, 3)->setTextAlignment(Qt::AlignCenter);
    }
    ui->clasMusicView->setModel(clasMusicItem);
    ui->clasMusicView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);//平均分配列宽
    ui->clasMusicView->setEditTriggers(QAbstractItemView::NoEditTriggers);  //不可编辑
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    if(index==1)
    {
        emit getArtistByNation(1);
    }
    if(index==2)
    {
        ui->clasListWidget->setCurrentRow(0);
    }
    if(index==3)
    {
        initPlayView(playList);
    }
}

void MainWindow::on_clasMusicView_doubleClicked(const QModelIndex &index) //曲风页面双击播放歌曲
{
    Music music=clasMusicList.at(index.row());
    QPixmap pix(":/images/pause.png");
    pix=pix.scaled(22,22);
    ui->btnPlaying->setIcon(QIcon(":/images/pause.png"));
    isPlaying=true;
    isPlayingName=QString("%1-%2").arg(music.getMusicName()).arg(music.getArtistName());
    timer->start(500);
    emit playByUrl(music.getFileUrl());
    if(isLogin)
    {
        emit insertListenRecord(token,music.getId());
        this->index=0;
    }

    if(playList.isEmpty())
    {
        playList.append(music);
    }else{
        int nowindex=0;
        for(Music amusic : playList)
        {
            nowindex++;
            if(amusic.getMusicName()==music.getMusicName())
            {
                this->index=nowindex;
                return;
            }
        }
        playList.append(music);
        this->index=playList.size()-1;
    }
}

void MainWindow::on_clasMusicView_customContextMenuRequested(const QPoint &pos)  //曲风页面歌曲右键菜单
{

    QMenu *cmenu = new QMenu(ui->clasMusicView);
    QAction *action1 = new QAction(tr("收藏歌曲"), this);
    QAction *action2 = new QAction(tr("下载歌曲"), this);
    cmenu->resize(50,30);
    action1->setData(1);
    cmenu->addAction(action1);
    cmenu->addAction(action2);
    // 下面这个on_menu_click(bool)槽函数做自己想做的事
    connect(action1, &QAction::triggered, this,[=](bool s){
        if(!s){
            if(isLogin==false)
            {
                QMessageBox::information(this,"提示","请先登录再进行收藏歌曲!");
                return;
            }else{
                Music rmusic=clasMusicList.at(ui->clasMusicView->currentIndex().row());
                for(Music music : songlistMusicList)
                {

                    if(music.getId()==rmusic.getId())
                    {
                        QMessageBox::information(this,"提示","该歌曲已在您的歌单中，请勿重复收藏!");
                        return;
                    }
                }
                emit insertMusicToSongList(rmusic.getId());
                QMessageBox::information(this,"提示","收藏成功❥(^_-)!");
            }
        }
    });
    connect(action2, &QAction::triggered, this,[=](bool s){
           Music rmusic=recommendMusicList.at(ui->recommedView->currentIndex().row());
           qDebug()<<rmusic.getMusicName();
           MusicNetworkImp *m=new MusicNetworkImp();
           m->downLoadMusic(rmusic.getFileUrl(),rmusic.getMusicName());
           QMessageBox::information(this,"提示","下载成功");
    });
    cmenu->exec(QCursor::pos());
}

void MainWindow::on_btnArtistInfoBack_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_btnLocalFile_clicked()   //添加本地音乐
{
    QStringList fileList=QFileDialog::getOpenFileNames(this,"选择文件","C:/Users/Administrator/Desktop/res/vedio","*.mp3;*.wav");
    if(fileList.size()>0)
    {
        this->localList.clear();
        localMusicItem->clear();
        for(QString str : fileList)
        {
            QFileInfo file(str);
            localList.append(str);
            localMusicItem->appendRow(new QStandardItem(file.fileName()));
        }
        ui->localMusicView->setModel(localMusicItem);
        ui->localMusicView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    }
}

void MainWindow::on_btnLocal_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
}

void MainWindow::on_localMusicView_doubleClicked(const QModelIndex &index)
{
    QString url=localList.at(index.row());
    QPixmap pix(":/images/pause.png");
    pix=pix.scaled(22,22);
    ui->btnPlaying->setIcon(QIcon(":/images/pause.png"));
    isPlaying=true;
    emit playByUrl(url);
}

void MainWindow::on_btnMain_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_comboBox_currentIndexChanged(const QString &arg1)   //登录下拉框
{
    if(isLogin==false)
    {
        if(ui->comboBox->currentText()=="登录")
        {
            loginDialog->show();
            ui->comboBox->setCurrentIndex(0);
        }
    }else{
        if(ui->comboBox->currentText()=="退出登录")
        {
            ui->comboBox->clear();
            ui->comboBox->addItem("欢迎━(*｀∀´*)ノ亻!");
            ui->comboBox->addItem("登录");
            ui->comboBox->setCurrentIndex(0);
            isLogin=false;
            ui->btnMySongList->setVisible(false);
            QPixmap pixmap(":/images/headboy.png");
            pixmap=pixmap.scaled(35,35);
            ui->headPhoto->setPixmap(pixmap);
            emit getAllMusic();
        }
        if(ui->comboBox->currentText()=="个人信息")
        {   ui->stackedWidget->setCurrentIndex(4);
            ui->comboBox->setCurrentIndex(0);
        }
        if(ui->comboBox->currentText()=="修改密码")
        {   ui->stackedWidget->setCurrentIndex(6);
            ui->comboBox->setCurrentIndex(0);
        }
    }
}

void MainWindow::initUserInfo(User newuser,QString newtoken)    //登录成功后的个人信息初始化
{
    qDebug()<<newuser.getPassWord();
    user=newuser;
    qDebug()<<user.getPassWord();
    token=newtoken;
    QString fileName=user.getHeadUrl().mid(1);
    QString path=QString("C:/Users/Public/MusicPlayer/Image//%1").arg(fileName);
    userfilePath=path;
    changefilePath=userfilePath;
    QPixmap pixmap(path);
    pixmap=pixmap.scaled(35,35);
    ui->headPhoto->setPixmap(pixmap);
    ui->comboBox->clear();
    ui->comboBox->addItem(user.getUserName());
    ui->comboBox->addItem("个人信息");
    ui->comboBox->addItem("修改密码");
    ui->comboBox->addItem("退出登录");
    ui->comboBox->setCurrentIndex(0);
    isLogin=true;
    QPixmap pix(path);
    pix=pix.scaled(250,250);
    ui->userHead->setPixmap(pix);
    ui->userName->setText(user.getUserName());
    ui->emailEdit->setText(user.getEmail());
    ui->nationCombox->setCurrentText(user.getLocation());
    ui->btnMySongList->setVisible(true);
    emit getRecommendMusuic(token);
}

void MainWindow::on_btnUserInfoBack_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_btnCancle_clicked()  //取消修改个人信息
{
    changefilePath=userfilePath;
    ui->nationCombox->setCurrentText(user.getLocation());
    ui->emailEdit->setText(user.getEmail());
    QPixmap pix(userfilePath);
    pix=pix.scaled(250,250);
    ui->userHead->setPixmap(pix);
    ui->userName->setText(user.getUserName());
    ui->emailEdit->setText(user.getEmail());
    ui->nationCombox->setCurrentText(user.getLocation());
}

void MainWindow::on_btnMySongList_clicked()
{
    emit getSongListMusic(token);
    ui->stackedWidget->setCurrentIndex(5);
}

void MainWindow::initMySongListView(QList<Music> list)   //歌单歌曲初始化页面
{
    songlistMusicList.clear();
    songlistMusicItem->clear();
    songlistMusicItem->setColumnCount(4);
    songlistMusicItem->setHeaderData(0,Qt::Horizontal, "音乐标题");
    songlistMusicItem->setHeaderData(1,Qt::Horizontal, "歌手");
    songlistMusicItem->setHeaderData(2,Qt::Horizontal, "专辑");
    songlistMusicItem->setHeaderData(3,Qt::Horizontal, "时长");
    for (int i=0;i<list.size();i++ ) {
        Music music=list.at(i);
        this->songlistMusicList.append(music);
        this->songlistMusicItem->setItem(i,0, new QStandardItem(music.getMusicName()));
        this->songlistMusicItem->setItem(i,1, new QStandardItem(music.getArtistName()) );
        this->songlistMusicItem->setItem(i,2, new QStandardItem(music.getAlbumName()) );
        this->songlistMusicItem->setItem(i,3, new QStandardItem(music.getMusicSize()) );
        this->songlistMusicItem->item(i, 0)->setTextAlignment(Qt::AlignCenter);
        this->songlistMusicItem->item(i, 1)->setTextAlignment(Qt::AlignCenter);
        this->songlistMusicItem->item(i, 2)->setTextAlignment(Qt::AlignCenter);
        this->songlistMusicItem->item(i, 3)->setTextAlignment(Qt::AlignCenter);
    }
    ui->songListMusicView->setModel(songlistMusicItem);
    ui->songListMusicView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);//平均分配列宽
    ui->songListMusicView->setEditTriggers(QAbstractItemView::NoEditTriggers);  //不可编辑

}

void MainWindow::on_songListMusicView_customContextMenuRequested(const QPoint &pos)  //歌单歌曲右键菜单
{

    QMenu *cmenu = new QMenu(ui->songListMusicView);
    QAction *action1 = new QAction(tr("删除歌曲"), this);
    QAction *action2 = new QAction(tr("下载歌曲"), this);
    cmenu->resize(50,30);
    action1->setData(1);
    cmenu->addAction(action1);
    cmenu->addAction(action2);
    // 下面这个on_menu_click(bool)槽函数做自己想做的事

    connect(action1, &QAction::triggered, this,[=](bool s){
            if(!s)
            {
                Music rmusic=songlistMusicList.at(ui->songListMusicView->currentIndex().row());
                emit deleteMusicFromList(rmusic.getId());
                QMessageBox::information(this,"提示","删除成功❥(^_-)!");
                emit getSongListMusic(token);
            }
        });
    cmenu->exec(QCursor::pos());
    connect(action2, &QAction::triggered, this,[=](bool s){
           Music rmusic=recommendMusicList.at(ui->recommedView->currentIndex().row());
           qDebug()<<rmusic.getMusicName();
           MusicNetworkImp *m=new MusicNetworkImp();
           m->downLoadMusic(rmusic.getFileUrl(),rmusic.getMusicName());
           QMessageBox::information(this,"提示","下载成功");
    });
}

void MainWindow::on_songListMusicView_doubleClicked(const QModelIndex &index)   //歌单歌曲双击播放
{
    Music music=songlistMusicList.at(index.row());
    QPixmap pix(":/images/pause.png");
    pix=pix.scaled(22,22);
    ui->btnPlaying->setIcon(QIcon(":/images/pause.png"));
    isPlaying=true;
    isPlayingName=QString("%1-%2").arg(music.getMusicName()).arg(music.getArtistName());
    timer->start(500);
    emit playByUrl(music.getFileUrl());
    if(isLogin)
    {
        emit insertListenRecord(token,music.getId());
        this->index=0;
    }

    if(playList.isEmpty())
    {
        playList.append(music);
    }else{
        int nowindex=0;
        for(Music amusic : playList)
        {
            nowindex++;
            if(amusic.getMusicName()==music.getMusicName())
            {
                this->index=nowindex;
                return;
            }
        }
        playList.append(music);
        this->index=playList.size()-1;
    }
}

void MainWindow::on_btnPlayMode_clicked()   //改变播放模式
{
    if(playMode==4)
    {
        playMode=1;
    }else {
        playMode++;
    }

    if(playMode==1)
    {
        ui->btnPlayMode->setIcon(QIcon(":/images/1 (1).png"));
        emit setPlayMode(1);
    }

    if(playMode==2)
    {
        ui->btnPlayMode->setIcon(QIcon(":/images/single (1).png"));
        emit setPlayMode(2);
    }

    if(playMode==3)
    {
        ui->btnPlayMode->setIcon(QIcon(":/images/single (2).png"));
        emit setPlayMode(1);
    }
    if(playMode==4)
    {
        ui->btnPlayMode->setIcon(QIcon(":/images/single (3).png"));
        emit setPlayMode(1);
    }
}

void MainWindow::on_btnChangeHead_clicked()  //改变头像按钮
{
    QStringList fileList=QFileDialog::getOpenFileNames(this,"请选择头像","C://Users//Public//MusicPlayer//Image//","*.png;*.jpg");
    if(fileList.size()==0)
    {
        return;
    }else if(fileList.size()>1)
    {
        QMessageBox::warning(this,"警告","只能选一个图片文件");
    }else{
        changefilePath=fileList.at(0);
        QPixmap pix(changefilePath);
        pix=pix.scaled(250,250);
        ui->userHead->setPixmap(pix);
    }
}

void MainWindow::on_btnChange_clicked()   //修改信息按钮
{
    QRegExp rx("^[a-z0-9A-Z]+[- | a-z0-9A-Z . _]+@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-z]{2,}$");
    if(!rx.exactMatch(ui->emailEdit->text()))
    {
        QMessageBox::warning(this,"警告","邮箱格式不正确");
        return;
    }
    if(userfilePath==changefilePath)
    {
        user.setLocation(ui->nationCombox->currentText());
        user.setEmail(ui->emailEdit->text());
        emit updateUser(user,token);
        QMessageBox::information(this,"成功","修改成功");
    }else{
        user.setLocation(ui->nationCombox->currentText());
        user.setEmail(ui->emailEdit->text());
        userfilePath=changefilePath;
        emit postImage(changefilePath);
    }
}

void MainWindow::getPostImageUrl(QString url)    //获取图片上传后获得的图片路径
{
    user.setHeadUrl(url);
    emit updateUser(user,token);
    QPixmap pixmap(userfilePath);
    pixmap=pixmap.scaled(35,35);
    ui->headPhoto->setPixmap(pixmap);
    QMessageBox::information(this,"成功","修改成功");
}

void MainWindow::on_btnChangePasswordBack_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_btnCanclechangePassword_clicked()
{
    ui->newPassword->setText("");
    ui->rnewpassword->setText("");
}

void MainWindow::on_btnChangePassword_clicked()
{
    if(ui->newPassword->text().length()<6){
        QMessageBox::warning(this,"警告","密码长度最少为6位");
        return;
    }
    if(ui->newPassword->text().isEmpty())
    {
         QMessageBox::warning(this,"警告","新密码不能为空！");
         return;
    }
    if(ui->newPassword->text()!=ui->rnewpassword->text())
    {
         QMessageBox::warning(this,"警告","两次密码输入不一致");
         return;
    }
    user.setPassWord(ui->newPassword->text());
    emit updateUser(user,token);
}

void MainWindow::initPlayView(QList<Music> list)  //初始化播放记录页
{
    playItem->clear();
    playItem->setColumnCount(4);
    playItem->setHeaderData(0,Qt::Horizontal, "音乐标题");
    playItem->setHeaderData(1,Qt::Horizontal, "歌手");
    playItem->setHeaderData(2,Qt::Horizontal, "专辑");
    playItem->setHeaderData(3,Qt::Horizontal, "时长");
    for (int i=0;i<list.size();i++ ) {
        Music music=list.at(i);
        this->playItem->setItem(i,0, new QStandardItem(music.getMusicName()));
        this->playItem->setItem(i,1, new QStandardItem(music.getArtistName()) );
        this->playItem->setItem(i,2, new QStandardItem(music.getAlbumName()) );
        this->playItem->setItem(i,3, new QStandardItem(music.getMusicSize()) );
        this->playItem->item(i, 0)->setTextAlignment(Qt::AlignCenter);
        this->playItem->item(i, 1)->setTextAlignment(Qt::AlignCenter);
        this->playItem->item(i, 2)->setTextAlignment(Qt::AlignCenter);
        this->playItem->item(i, 3)->setTextAlignment(Qt::AlignCenter);
    }
    ui->playView->setModel(playItem);
    ui->playView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);//平均分配列宽
    ui->playView->setEditTriggers(QAbstractItemView::NoEditTriggers);  //不可编辑
}

void MainWindow::on_playView_doubleClicked(const QModelIndex &index)
{
    Music music=playList.at(index.row());
    QPixmap pix(":/images/pause.png");
    pix=pix.scaled(22,22);
    ui->btnPlaying->setIcon(QIcon(":/images/pause.png"));
    isPlaying=true;
    isPlayingName=QString("%1-%2").arg(music.getMusicName()).arg(music.getArtistName());
    timer->start(500);
    emit playByUrl(music.getFileUrl());
}

void MainWindow::on_playView_customContextMenuRequested(const QPoint &pos)
{
    QMenu *cmenu = new QMenu(ui->clasMusicView);
    QAction *action1 = new QAction(tr("收藏歌曲"), this);
    QAction *action2 = new QAction(tr("下载歌曲"), this);
    cmenu->resize(50,30);
    action1->setData(1);
    cmenu->addAction(action1);
    cmenu->addAction(action2);
    // 下面这个on_menu_click(bool)槽函数做自己想做的事
    connect(action1, &QAction::triggered, this,[=](bool s){
        if(!s){
            if(isLogin==false)
            {
                QMessageBox::information(this,"提示","请先登录再进行收藏歌曲!");
                return;
            }else{
                Music rmusic=clasMusicList.at(ui->clasMusicView->currentIndex().row());
                for(Music music : songlistMusicList)
                {

                    if(music.getId()==rmusic.getId())
                    {
                        QMessageBox::information(this,"提示","该歌曲已在您的歌单中，请勿重复收藏!");
                        return;
                    }
                }
                emit insertMusicToSongList(rmusic.getId());
                QMessageBox::information(this,"提示","收藏成功❥(^_-)!");
            }
        }
    });
    connect(action2, &QAction::triggered, this,[=](bool s){
           Music rmusic=recommendMusicList.at(ui->recommedView->currentIndex().row());
           qDebug()<<rmusic.getMusicName();
           MusicNetworkImp *m=new MusicNetworkImp();
           m->downLoadMusic(rmusic.getFileUrl(),rmusic.getMusicName());
           QMessageBox::information(this,"提示","下载成功");
    });
    cmenu->exec(QCursor::pos());
}


void MainWindow::on_playingPosition_sliderMoved(int position)
{
    emit setPlayPosition(position);
}

