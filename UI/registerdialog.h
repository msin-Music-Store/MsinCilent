#ifndef REGISTERDIALOG_H
#define REGISTERDIALOG_H

#include <QDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <Controller/usercontrol.h>
#include "DataAccess/User/mysonglistnetwork.h"
#include <QRegExp>
namespace Ui {
class RegisterDialog;
}

class RegisterDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RegisterDialog(QWidget *parent = nullptr);
    ~RegisterDialog();

private slots:
    void on_btnFile_clicked();

    void on_btnRegister_clicked();

    void setImageUrl(QString);
    void getRegisterResult(bool);
    void on_btnCancle_clicked();

private:
    Ui::RegisterDialog *ui;
    QString token;
    QString fileName;
    UserControl *userControl=new UserControl(this);
    MysongListNetwork *mySongList=new MysongListNetwork(this);
    User user;
};

#endif // REGISTERDIALOG_H
