#include "logindialog.h"
#include "ui_logindialog.h"


LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog)
{
    ui->setupUi(this);
    this->setWindowTitle("登录");
    this->setWindowIcon(QIcon(":/images/MusicPlayer.png"));
    registerDialog=new RegisterDialog(this);

    userControl->moveToThread(userThread);
    userThread->start();

    connect(this,&LoginDialog::login,userControl,&UserControl::getLogin);
    connect(userControl,&UserControl::getLoginInfo,this,&LoginDialog::getLoginInfo);
    connect(this,&LoginDialog::getUser,userControl,&UserControl::getUser);
    connect(userControl,&UserControl::getUserInfo,this,&LoginDialog::getUserInfo);
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

void LoginDialog::on_btnLogin_clicked()
{
    if(ui->userNameEdit->text().isEmpty()||ui->passwordEdit->text().isEmpty())
    {
        QMessageBox::warning(this,"警告","账号或密码不能为空！");
    }
    else{
        emit login(ui->userNameEdit->text(),ui->passwordEdit->text());
    }
}
void LoginDialog::getLoginInfo(QStringList list)
{
    if(list.at(0)=="200")
    {
        token=list.at(1);
        emit getUser(token);
    }else{
        QMessageBox::warning(this,"警告","账号或密码错误！");
    }
}

void LoginDialog::getUserInfo(User newuser)
{
    user=newuser;
    user.setPassWord(ui->passwordEdit->text());
    emit loginSucceed(user,token);
    ui->userNameEdit->setText("");
    ui->passwordEdit->setText("");
    this->close();
}


void LoginDialog::on_btnRegister_clicked()
{
    registerDialog->show();
}

void LoginDialog::deleteThread()
{
    userThread->quit();
    userThread->wait(5);
    delete userControl;
    delete registerDialog;
}
