#include "registerdialog.h"
#include "ui_registerdialog.h"
#include <QIcon>

RegisterDialog::RegisterDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RegisterDialog)
{
    ui->setupUi(this);
    this->setWindowTitle("注册");
    this->setWindowIcon(QIcon(":/images/MusicPlayer.png"));
    connect(userControl,&UserControl::getPostImageUrl,this,&RegisterDialog::setImageUrl);
    connect(userControl,&UserControl::getRegisterResult,this,&RegisterDialog::getRegisterResult);
}

RegisterDialog::~RegisterDialog()
{
    delete ui;
}

void RegisterDialog::on_btnFile_clicked()
{
    QStringList filelist=QFileDialog::getOpenFileNames(this,"选择图片文件","C://Users//Public//MusicPlayer//Image","*.png;*.jpg");
    if(filelist.size()==0)
    {
        return;
    }else if(filelist.length()>1){
        QMessageBox::warning(this,"警告","一次只能选择一个文件");
        return;
    }else{
        fileName=filelist.at(0);
        QPixmap pix(fileName);
        pix=pix.scaled(150,150);
        ui->registerHead->setPixmap(pix);
    }
}


void RegisterDialog::on_btnRegister_clicked()
{
    QRegExp rx("^[a-z0-9A-Z]+[- | a-z0-9A-Z . _]+@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-z]{2,}$");
    if(!rx.exactMatch(ui->remailEdit->text()))
    {
        QMessageBox::warning(this,"警告","邮箱格式不正确");
        return;
    }
    if(ui->rusernameEdit->text().isEmpty())
    {
        QMessageBox::warning(this,"警告","用户名不能为空");
        return;
    }
    if(ui->remailEdit->text().isEmpty()){
        QMessageBox::warning(this,"警告","请输入邮箱");
        return;
    }
    if(ui->rpassword->text().isEmpty()){
        QMessageBox::warning(this,"警告","请输入密码");
        return;
    }
    if(ui->rrpassword->text().isEmpty()){
        QMessageBox::warning(this,"警告","请确认密码");
        return;
    }
    if(ui->rrpassword->text()!=ui->rpassword->text()){
        QMessageBox::warning(this,"警告","两次密码输入不一致");
        return;
    }
    if(fileName.isEmpty())
    {
        QMessageBox::warning(this,"警告","请选择头像");
        return;
    }
    if(ui->rnationComBox->currentText()=="请选择")
    {
        QMessageBox::warning(this,"警告","请选择国籍");
        return;
    }

    userControl->getPostImage(fileName);
}

void RegisterDialog::setImageUrl(QString url)
{
    user.setHeadUrl(url);
    user.setUserName(ui->rusernameEdit->text());
    user.setLocation(ui->rnationComBox->currentText());
    user.setEmail(ui->remailEdit->text());
    user.setPassWord(ui->rpassword->text());
    userControl->registerUser(user);
}
void RegisterDialog::getRegisterResult(bool s)
{
    if(s){
        QMessageBox::information(this,"成功","注册成功");
        userControl->getLogin(ui->rusernameEdit->text(),ui->rpassword->text());
        this->close();
        ui->remailEdit->setText("");
        ui->rnationComBox->setCurrentIndex(0);
        ui->rusernameEdit->setText("");
        ui->rpassword->setText("");
        ui->rrpassword->setText("");
        QPixmap pix(":/images/head.boy.png");
        pix=pix.scaled(150,150);
        ui->registerHead->setPixmap(pix);

    }else{
        QMessageBox::warning(this,"警告","用户名已存在，请重试！");
    }
}


void RegisterDialog::on_btnCancle_clicked()
{
    ui->remailEdit->setText("");
    ui->rnationComBox->setCurrentIndex(0);
    ui->rusernameEdit->setText("");
    ui->rpassword->setText("");
    ui->rrpassword->setText("");
    QPixmap pix(":/images/head.boy.png");
    pix=pix.scaled(150,150);
    ui->registerHead->setPixmap(pix);
    this->close();
}

